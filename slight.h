#include "shell.h"

#ifndef TDSH_SLIGHT_H
#define TDSH_SLIGHT_H

int shell_make_slights(struct DemoShell* shell);

int variable_is_slight(const struct Variable* var);
struct SpotLight* variable_to_slight(const struct Variable* var);
int variable_set_slight_index(const struct Variable* var, unsigned int i);
int variable_make_slight(struct DemoShell* shell, const struct SpotLight* sl, struct Variable* dest);

#endif
