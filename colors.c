#include <stdio.h>
#include <math.h>
#include <3dmr/math/linear_algebra.h>
#include "builtins.h"
#include "variable.h"

static void hsv2rgb(double h, double s, double v, Vec3 dest) {
    double p, q, t, rem;
    long i;

    if (s == 0.0) {
        dest[0] = v;
        dest[1] = v;
        dest[2] = v;
    } else {
        h /= 60.0;
        i = (long)h;
        rem = h - i;
        p = v * (1.0 - s);
        q = v * (1.0 - (s * rem));
        t = v * (1.0 - (s * (1.0 - rem)));

        switch (i % 6) {
            case 0: dest[0] = v; dest[1] = t; dest[2] = p; break;
            case 1: dest[0] = q; dest[1] = v; dest[2] = p; break;
            case 2: dest[0] = p; dest[1] = v; dest[2] = t; break;
            case 3: dest[0] = p; dest[1] = q; dest[2] = v; break;
            case 4: dest[0] = t; dest[1] = p; dest[2] = v; break;
            case 5: dest[0] = v; dest[1] = p; dest[2] = q; break;
        }
    }
}

static int hsv2rgb_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec3 hsv;

    if (nArgs == 1) {
        if (!variable_to_vec3(args, hsv)) {
            fprintf(stderr, "Error: hsv2rgb(): expected vec3 or 3 floats\n");
            return 0;
        }
    } else if (nArgs == 3) {
        unsigned int i;
        for (i = 0; i < 3; i++) {
            if (!variable_to_float(args + i, &hsv[i])) {
                fprintf(stderr, "Error: hsv2rgb(): expected vec3 or 3 floats\n");
                return 0;
            }
        }
    } else {
        fprintf(stderr, "Error: hsv2rgb(): expected vec3 or 3 floats\n");
        return 0;
    }
    hsv2rgb(hsv[0], hsv[1], hsv[2], ret->data.dvec3);
    ret->type = VEC3;
    return 1;
}

void shell_colors_init(struct ShellColors* colors) {
    colors->hue = 0;
}

static const struct PointerInfo vec3Ptr = {vec3_ptr_get, NULL, NULL, NULL};

int shell_make_color_vars(struct DemoShell* shell) {
    return builtin_function(shell, "hsv2rgb", hsv2rgb_)
        && builtin_ptr(shell, "multi", &vec3Ptr, shell->colors.colors[SHELL_COLOR_MULTI], NULL);
}

void shell_update_colors(struct ShellColors* shcolors, double dt) {
    shcolors->hue = fmod(shcolors->hue + 50.0 * dt, 360.0);
    hsv2rgb(shcolors->hue, 1.0, 1.0, shcolors->colors[SHELL_COLOR_MULTI]);
}

int variable_is_multi(const struct Variable* var) {
    return var->type == POINTER && var->data.dpointer.info == &vec3Ptr;
}
