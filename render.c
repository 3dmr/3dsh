#include <3dmr/render/camera_buffer_object.h>
#include <3dmr/render/lights_buffer_object.h>
#include <3dmr/skybox.h>
#include "render.h"
#include "shell.h"
#include "node.h"
#include "camera.h"

static void update_node(struct Scene* scene, struct Node* n, void* data) {
    struct DemoShell* shell = data;
    unsigned int i;

    switch (n->type) {
        case NODE_DLIGHT:
            for (i = 0; i < shell->numDirectionalLights; i++) {
                if (shell->rdlight[i] && shell->rdlight[i] == node_get_var_data(n)->data.light.data.dstruct.data) {
                    lights_buffer_object_update_dlight(&scene->lights, n->data.dlight, i);
                    shell->lightsChanged = 1;
                }
            }
            break;
        case NODE_PLIGHT:
            for (i = 0; i < shell->numPointLights; i++) {
                if (shell->rplight[i] && shell->rplight[i] == node_get_var_data(n)->data.light.data.dstruct.data) {
                    lights_buffer_object_update_plight(&scene->lights, n->data.plight, i);
                    shell->lightsChanged = 1;
                }
            }
            break;
        case NODE_SLIGHT:
            for (i = 0; i < shell->numSpotLights; i++) {
                if (shell->rslight[i] && shell->rslight[i] == node_get_var_data(n)->data.light.data.dstruct.data) {
                    lights_buffer_object_update_slight(&scene->lights, n->data.slight, i);
                    shell->lightsChanged = 1;
                }
            }
            break;
        case NODE_CAMERA:
            {
                struct Camera* camera = variable_to_camera(&shell->activeCamera);
                if (n->data.camera == camera) {
                    camera_buffer_object_update_view(&scene->camera, MAT_CONST_CAST(camera->view));
                    camera_buffer_object_update_position(&scene->camera, n->position);
                    shell->cameraChanged = 1;
                }
            }
            break;
        default:;
    }
}

int update_scene(struct DemoShell* shell) {
    struct Camera* activecam;
    int changed;
    if (!(activecam = variable_to_camera(&shell->activeCamera))) return 0;
    changed = shell->cameraChanged | shell->graphChanged;
    if (scene_update_nodes(&shell->scene, update_node, shell) || changed) {
        scene_update_render_queue(&shell->scene, MAT_CONST_CAST(activecam->view), MAT_CONST_CAST(activecam->projection));
    }
    shell->graphChanged = 0;
    return 1;
}

int render_frame(struct DemoShell* shell) {
    if (!update_scene(shell)) return 0;
    if (shell->lightsChanged) {
        uniform_buffer_send(&shell->scene.lights);
        shell->lightsChanged = 0;
    }
    if (shell->cameraChanged) {
        uniform_buffer_send(&shell->scene.camera);
        shell->cameraChanged = 0;
    }
    if (shell->skybox) {
        skybox_render(shell->skybox);
    }
    scene_render(&shell->scene);
    return 1;
}

void* shell_frame_swap_thread(void* data) {
    struct DemoShell* shell = data;
    int running = 1;

    while (running) {
        if (!pthread_mutex_lock(&shell->swapMutex)) {
            while (running && !shell->renderDone) {
                pthread_cond_signal(&shell->mainCond);
                pthread_cond_wait(&shell->swapCond, &shell->swapMutex);
                if (!pthread_mutex_trylock(&shell->mainMutex)) {
                    running = shell->running;
                    pthread_mutex_unlock(&shell->mainMutex);
                }
            }
            if (running && shell->renderDone) {
                viewer_make_current(shell->viewer);
                shell->dt = viewer_next_frame(shell->viewer);
                viewer_make_current(NULL);
                shell->renderDone = 0;
                pthread_cond_signal(&shell->mainCond);
            }
            pthread_mutex_unlock(&shell->swapMutex);
        }
        if (!pthread_mutex_trylock(&shell->mainMutex)) {
            running = shell->running;
            pthread_mutex_unlock(&shell->mainMutex);
        }
    }
    return NULL;
}
