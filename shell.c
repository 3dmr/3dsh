#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <3dmr/render/viewer.h>
#include <3dmr/skybox.h>
#include "callbacks.h"
#include "eval.h"
#include "shell.h"

void shell_config_default(struct ShellConfig* config) {
    config->defWidth = 1024;
    config->defHeight = 768;
    config->execCmd = NULL;
    config->argv = NULL;
    config->argc = 0;
}

int shell_init(struct DemoShell* shell, const struct ShellConfig* config) {
    const char* ps1 = getenv("DEMO_PROMPT");
    const char* ps2 = "... ";
    unsigned int i;

    if (!ps1) ps1 = "demo> ";

    zero3v(shell->ambientLight.color);
    shell->numDirectionalLights = 0;
    shell->numPointLights = 0;
    shell->numSpotLights = 0;
    shell->ibl.enabled = 0;
    shell->skybox = NULL;
    shell->refcount = NULL;
    shell->numRefcounts = 0;
    shell->contexts = NULL;
    shell->numContexts = 0;
    shell->config = *config;
    shell->fps = 0;
    shell->running = 1;
    shell->interactive = 0;
    shell->cameraChanged = 0;
    shell->lightsChanged = 0;
    shell->graphChanged = 0;
    shell_colors_init(&shell->colors);
    shell->activeCamera.type = VOID;
    shell->mouseCb.type = VOID;
    shell->scrollCb.type = VOID;
    shell->keyCb.type = VOID;
    shell->resizeCb.type = VOID;
    if (!(shell->ps1 = malloc(strlen(ps1) + 1))
     || !(shell->ps2 = malloc(strlen(ps2) + 1))) {
        free(shell->ps1);
        return 0;
    }
    for (i = 0; i < MAX_DIRECTIONAL_LIGHTS; i++) {
        shell->rdlight[i] = NULL;
    }
    for (i = 0; i < MAX_POINT_LIGHTS; i++) {
        shell->rplight[i] = NULL;
    }
    for (i = 0; i < MAX_SPOT_LIGHTS; i++) {
        shell->rslight[i] = NULL;
    }
    shell->curFile = "";
    shell->curLine = NULL;
    shell->curLineNo = 0;
    strcpy(shell->ps1, ps1);
    strcpy(shell->ps2, ps2);
    shell->prompt = shell->ps1;
    shell->dt = 0.0;
    shell->renderDone = 0;
    shell->enableRender = 1;
    shell->enableExec = 1;
    shell->lasterr = 0;
    if (!pthread_mutex_init(&shell->mainMutex, NULL)) {
        if (!pthread_cond_init(&shell->mainCond, NULL)) {
            if (!pthread_mutex_init(&shell->inputMutex, NULL)) {
                if (!pthread_cond_init(&shell->inputCond, NULL)) {
                    if (!pthread_mutex_init(&shell->swapMutex, NULL)) {
                        if (!pthread_cond_init(&shell->swapCond, NULL)) {
                            if (shell_push_context(shell)) {
                                if ((shell->viewer = viewer_new(shell->config.defWidth, shell->config.defHeight, "Demo"))) {
                                    shell->viewer->key_callback = callback_key;
                                    shell->viewer->cursor_callback = callback_mouse;
                                    shell->viewer->wheel_callback = callback_wheel;
                                    shell->viewer->resize_callback = callback_resize;
                                    shell->viewer->close_callback = callback_close;
                                    shell->viewer->callbackData = shell;
                                    scene_init(&shell->scene, NULL);
                                    glfwSwapInterval(1);
                                    viewer_next_frame(shell->viewer);
                                    return 1;
                                }
                                shell_pop_context(shell);
                            }
                            pthread_cond_destroy(&shell->swapCond);
                        }
                        pthread_mutex_destroy(&shell->swapMutex);
                    }
                    pthread_cond_destroy(&shell->inputCond);
                }
                pthread_mutex_destroy(&shell->inputMutex);
            }
            pthread_cond_destroy(&shell->mainCond);
        }
        pthread_mutex_destroy(&shell->mainMutex);
    }
    free(shell->ps1);
    free(shell->ps2);
    return 0;
}

void shell_free(struct DemoShell* shell) {
    unsigned int i;

    viewer_make_current(shell->viewer);
    for (i = 0; i < MAX_DIRECTIONAL_LIGHTS; i++) {
        if (shell->rdlight[i]) shell_decref(shell->rdlight[i]);
    }
    for (i = 0; i < MAX_POINT_LIGHTS; i++) {
        if (shell->rplight[i]) shell_decref(shell->rplight[i]);
    }
    for (i = 0; i < MAX_SPOT_LIGHTS; i++) {
        if (shell->rslight[i]) shell_decref(shell->rslight[i]);
    }
    variable_free(&shell->activeCamera);
    shell_free_variables(shell);
    scene_free(&shell->scene, NULL);
    skybox_free(shell->skybox);
    viewer_make_current(NULL);
    viewer_free(shell->viewer);
    for (i = 0; i < shell->numRefcounts; i++) {
        free(shell->refcount[i]);
    }
    free(shell->refcount);
    free(shell->contexts);
    pthread_mutex_destroy(&shell->mainMutex);
    pthread_cond_destroy(&shell->mainCond);
    pthread_mutex_destroy(&shell->inputMutex);
    pthread_cond_destroy(&shell->inputCond);
    pthread_mutex_destroy(&shell->swapMutex);
    pthread_cond_destroy(&shell->swapCond);
    free(shell->ps1);
    free(shell->ps2);
}

struct ShellVarNode* shell_active_context(struct DemoShell* shell) {
    if (!shell->numContexts) return NULL;
    return shell->contexts + (shell->numContexts - 1);
}

struct ShellVarNode* shell_push_context(struct DemoShell* shell) {
    struct ShellVarNode* tmp;
    if (!(tmp = realloc(shell->contexts, (shell->numContexts + 1) * sizeof(*tmp)))) return NULL;
    shell->contexts = tmp;
    tmp += shell->numContexts++;
    tmp->value.children = NULL;
    tmp->numChildren = 0;
    tmp->c = 0;
    return tmp;
}

void shell_pop_context(struct DemoShell* shell) {
    struct ShellVarNode *stack[256], *root, *cur, *next;
    unsigned int i, numStack = 0;

    if (!(root = shell_active_context(shell))) return;

    while (root->numChildren) {
        stack[numStack++] = root;
        while (numStack) {
            cur = stack[--numStack];
            i = 0;
            while (i < cur->numChildren && numStack < sizeof(stack) / sizeof(*stack)) {
                next = &cur->value.children[i];
                if (next->numChildren == 0) {
                    if (next->c) {
                        free(next->value.children);
                    } else {
                        variable_free(next->value.var);
                        free(next->value.var);
                    }
                    *next = cur->value.children[--cur->numChildren];
                } else {
                    stack[numStack++] = next;
                    i++;
                }
            }
        }
    }
    free(root->value.children);
    shell->numContexts--;
}

struct Variable* shell_add_variable(struct DemoShell* shell, const char* name) {
    struct ShellVarNode *cur, *next;
    unsigned int i;

    if (!*name || !(cur = shell_active_context(shell))) return NULL;
    do {
        next = NULL;
        for (i = 0; i < cur->numChildren; i++) {
            if (cur->value.children[i].c == *name) {
                if (!*name) return 0;
                next = cur->value.children + i;
                break;
            }
        }
        if (!next) {
            if (!(next = realloc(cur->value.children, (cur->numChildren + 1) * sizeof(*next)))) {
                return 0;
            }
            cur->value.children = next;
            next += cur->numChildren++;
            next->value.children = NULL;
            next->numChildren = 0;
            next->c = *name;
        }
        cur = next;
    } while (*name++);
    return cur->value.var = malloc(sizeof(*cur->value.var));
}

struct Variable* shell_get_variable(struct DemoShell* shell, const char* name, int recursive) {
    struct ShellVarNode *cur, *next;
    const char* n;
    unsigned int i, context;

    if (!*name) return NULL;
    for (context = shell->numContexts; context; context--) {
        cur = shell->contexts + context - 1;
        n = name;
        do {
            next = NULL;
            for (i = 0; i < cur->numChildren; i++) {
                if (cur->value.children[i].c == *n) {
                    next = cur->value.children + i;
                    break;
                }
            }
            if (!next) {
                goto not_found;
            }
            cur = next;
        } while (*n++);
        return cur->value.var;
        not_found:
        if (!recursive) break;
    }
    return NULL;
}

void shell_del_variable(struct DemoShell* shell, const char* name) {
    struct ShellVarNode *cur, *next, *prev;
    unsigned int i;

    if (!*name || !(cur = shell_active_context(shell))) return;
    do {
        next = NULL;
        prev = cur;
        for (i = 0; i < cur->numChildren; i++) {
            if (cur->value.children[i].c == *name) {
                next = cur->value.children + i;
                break;
            }
        }
        if (!next) {
            return;
        }
        cur = next;
    } while (*name++);
    variable_free(cur->value.var);
    free(cur->value.var);
    prev->value.children[i] = prev->value.children[--(prev->numChildren)];
}

void shell_free_variables(struct DemoShell* shell) {
    while (shell->numContexts) {
        shell_pop_context(shell);
    }
}

struct ShellRefCount* shell_new_refcount(struct DemoShell* shell, void (*destroy)(struct DemoShell*, void*), void* data) {
    struct ShellRefCount** r;
    unsigned int i;

    for (i = 0; i < shell->numRefcounts; i++) {
        if (!shell->refcount[i]) {
            break;
        }
    }
    if (i >= shell->numRefcounts) {
        if (shell->numRefcounts >= ((unsigned int)-2)
         || !(r = realloc(shell->refcount, (shell->numRefcounts + 1) * sizeof(*r)))) {
            return NULL;
        }
        shell->refcount = r;
        i = shell->numRefcounts++;
        shell->refcount[i] = NULL;
    }
    if (!shell->refcount[i]) {
        if (!(shell->refcount[i] = malloc(sizeof(*shell->refcount[i])))) {
            return NULL;
        }
    }
    shell->refcount[i]->destroy = destroy;
    shell->refcount[i]->shell = shell;
    shell->refcount[i]->data = data;
    shell->refcount[i]->refcount = 1;
    return shell->refcount[i];
}

int shell_incref(struct ShellRefCount* r) {
    if (r->refcount < ((unsigned int)-1)) {
        r->refcount++;
        return 1;
    }
    return 0;
}

void shell_decref(struct ShellRefCount* r) {
    struct DemoShell* shell = r->shell;
    unsigned int i;

    if (r->refcount > 1) {
        r->refcount--;
        return;
    }
    for (i = 0; i < shell->numRefcounts; i++) {
        if (shell->refcount[i] == r) {
            shell->refcount[i] = NULL;
        }
    }
    if (r->destroy) r->destroy(shell, r->data);
    free(r);
}
