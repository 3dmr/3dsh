#include "shell.h"

#ifndef TDSH_FUNCTIONS_MATH_H
#define TDSH_FUNCTIONS_MATH_H

int shell_make_math_functions(struct DemoShell* shell);

#endif
