#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <3dmr/math/linear_algebra.h>
#include <3dmr/render/camera.h>
#include <3dmr/render/camera_buffer_object.h>
#include <3dmr/render/viewer.h>
#include "builtins.h"
#include "callbacks.h"
#include "shell.h"
#include "camera.h"
#include "render.h"

void callback_mouse(struct Viewer* viewer, double xpos, double ypos, double dx, double dy, int buttonLeft, int buttonMiddle, int buttonRight, void* userData) {
    struct Variable ret;
    struct DemoShell* shell = userData;
    if (shell->mouseCb.type == FUNCTION && variable_function_call_l(&shell->mouseCb, &ret, "%f%f%f%f%d%d%d", xpos, ypos, dx, dy, buttonLeft, buttonMiddle, buttonRight)) {
        variable_free(&ret);
    }
}

static int default_callback_mouse(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Quaternion q, orient, tmp;
    Vec3 axis = {0, 1, 0}, pos;
    struct DemoShell* shell = data;
    struct Viewer* viewer = shell->viewer;
    struct Camera* camera = variable_to_camera(&shell->activeCamera);
    struct Node* node;
    long buttonLeft;
    float dx, dy;

    if (nArgs != 7 || !variable_to_float(args + 2, &dx) || !variable_to_float(args + 3, &dy) || !variable_to_int(args + 4, &buttonLeft)) {
        return 0;
    }
    if (buttonLeft && camera) {
        node = variable_to_camera_node(&shell->activeCamera);
        quaternion_set_axis_angle(q, axis, dx / viewer->width);
        camera_get_right(MAT_CONST_CAST(camera->view), axis);
        quaternion_compose(axis, q, axis);
        if (node) {
            node_rotate_q(node, q);
        } else {
            camera_get_position(MAT_CONST_CAST(camera->view), pos);
            camera_get_orientation(MAT_CONST_CAST(camera->view), orient);
            quaternion_mul(tmp, q, orient);
        }
        quaternion_set_axis_angle(q, axis, dy / viewer->height);
        if (node) {
            node_rotate_q(node, q);
        } else {
            quaternion_mul(orient, q, tmp);
            camera_view(pos, orient, camera->view);
            camera_buffer_object_update_view(&shell->scene.camera, MAT_CONST_CAST(camera->view));
            shell->cameraChanged = 1;
        }
    }
    ret->type = VOID;
    return 1;
}

void callback_wheel(struct Viewer* viewer, double xoffset, double yoffset, void* userData) {
    struct Variable ret;
    struct DemoShell* shell = userData;
    if (shell->scrollCb.type == FUNCTION && variable_function_call_l(&shell->scrollCb, &ret, "%f%f", xoffset, yoffset)) {
        variable_free(&ret);
    }
}

static int default_callback_wheel(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec3 axis;
    struct DemoShell* shell = data;
    struct Camera* camera = variable_to_camera(&shell->activeCamera);
    struct Node* node;
    float yoffset;

    if (nArgs != 2 || !variable_to_float(args + 1, &yoffset)) {
        return 0;
    }
    if (camera) {
        camera_get_backward(MAT_CONST_CAST(camera->view), axis);
        scale3v(axis, -yoffset / 10.0);
        if ((node = variable_to_camera_node(&shell->activeCamera))) {
            node_translate(node, axis);
        } else {
            Quaternion orient;
            Vec3 pos;
            camera_get_position(MAT_CONST_CAST(camera->view), pos);
            camera_get_orientation(MAT_CONST_CAST(camera->view), orient);
            incr3v(pos, axis);
            camera_view(pos, orient, camera->view);
            camera_buffer_object_update_view_and_position(&shell->scene.camera, MAT_CONST_CAST(camera->view));
            shell->cameraChanged = 1;
        }
    }
    ret->type = VOID;
    return 1;
}

static void do_close(struct DemoShell* shell) {
    if (pthread_mutex_lock(&shell->mainMutex)) return;
    shell->running = 0;
    pthread_mutex_unlock(&shell->mainMutex);
}

void callback_key(struct Viewer* viewer, int key, int scancode, int action, int mods, void* userData) {
    struct Variable ret;
    struct DemoShell* shell = userData;
    if (shell->keyCb.type == FUNCTION && variable_function_call_l(&shell->keyCb, &ret, "%d%d%d%d", key, scancode, action, mods)) {
        variable_free(&ret);
    }
}

static int default_callback_key(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec3 axis = {0, 0, 0};
    struct DemoShell* shell = data;
    struct Camera* camera = NULL;
    struct Node* node;
    long key;

    if (nArgs != 4 || !variable_to_int(args, &key)) {
        return 0;
    }
    switch (key) {
        case GLFW_KEY_ESCAPE:
            do_close(shell);
            break;
        case GLFW_KEY_PAGE_UP:
        case GLFW_KEY_Q:
            if ((camera = variable_to_camera(&shell->activeCamera))) {
                camera_get_up(MAT_CONST_CAST(camera), axis);
                scale3v(axis, 0.1);
            }
            break;
        case GLFW_KEY_PAGE_DOWN:
        case GLFW_KEY_E:
            if ((camera = variable_to_camera(&shell->activeCamera))) {
                camera_get_up(MAT_CONST_CAST(camera), axis);
                scale3v(axis, -0.1);
            }
            break;
        case GLFW_KEY_LEFT:
        case GLFW_KEY_A:
            if ((camera = variable_to_camera(&shell->activeCamera))) {
                camera_get_right(MAT_CONST_CAST(camera), axis);
                scale3v(axis, -0.1);
            }
            break;
        case GLFW_KEY_RIGHT:
        case GLFW_KEY_D:
            if ((camera = variable_to_camera(&shell->activeCamera))) {
                camera_get_right(MAT_CONST_CAST(camera), axis);
                scale3v(axis, 0.1);
            }
            break;
        case GLFW_KEY_DOWN:
        case GLFW_KEY_S:
            if ((camera = variable_to_camera(&shell->activeCamera))) {
                camera_get_backward(MAT_CONST_CAST(camera), axis);
                scale3v(axis, 0.1);
            }
            break;
        case GLFW_KEY_UP:
        case GLFW_KEY_W:
            if ((camera = variable_to_camera(&shell->activeCamera))) {
                camera_get_backward(MAT_CONST_CAST(camera), axis);
                scale3v(axis, -0.1);
            }
            break;
        case GLFW_KEY_F12:
            viewer_screenshot(shell->viewer, "screenshot.png");
            break;
    }
    if (camera) {
        if ((node = variable_to_camera_node(&shell->activeCamera))) {
            node_translate(node, axis);
        } else {
            Quaternion orient;
            Vec3 pos;
            camera_get_position(MAT_CONST_CAST(camera->view), pos);
            camera_get_orientation(MAT_CONST_CAST(camera->view), orient);
            incr3v(pos, axis);
            camera_view(pos, orient, camera->view);
            camera_buffer_object_update_view_and_position(&shell->scene.camera, MAT_CONST_CAST(camera->view));
            shell->cameraChanged = 1;
        }
    }
    ret->type = VOID;
    return 1;
}

void callback_resize(struct Viewer* viewer, void* userData) {
    struct Variable ret;
    struct DemoShell* shell = userData;
    if (!pthread_mutex_lock(&shell->swapMutex)) {
        viewer_make_current(shell->viewer);
        glViewport(0, 0, shell->viewer->width, shell->viewer->height);
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    if (shell->resizeCb.type == FUNCTION && variable_function_call_l(&shell->resizeCb, &ret, "%d%d", shell->viewer->width, shell->viewer->height)) {
        variable_free(&ret);
    }
}

static int default_callback_resize(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct Camera* camera;
    struct Projection* proj;
    if ((camera = variable_to_camera(&shell->activeCamera))) {
        float ratio = ((float)shell->viewer->width) / ((float)shell->viewer->height);
        camera_set_ratio(ratio, camera->projection);
        if ((proj = variable_to_camera_proj(&shell->activeCamera))) {
            proj->ratio = ratio;
        }
        camera_buffer_object_update_projection(&shell->scene.camera, MAT_CONST_CAST(camera->projection));
        shell->cameraChanged = 1;
    }
    ret->type = VOID;
    return 1;
}

void callback_close(struct Viewer* viewer, void* userData) {
    do_close(userData);
}

int cb_get(const void* src, struct Variable* dest) {
    return variable_copy(src, dest);
}

int cb_set(const struct Variable* src, void* dest) {
    struct Variable new;
    if (!variable_copy_dereference(src, &new)) return 0;
    if (new.type != FUNCTION) {
        variable_free(&new);
        return 0;
    }
    variable_free(dest);
    *((struct Variable*)dest) = new;
    return 1;
}

static const struct PointerInfo cbPtr = {cb_get, cb_set, NULL, NULL};

int shell_make_callbacks(struct DemoShell* shell) {
    variable_make_function(default_callback_mouse, shell, FUNC_SIMPLE_COPY, &shell->mouseCb);
    variable_make_function(default_callback_wheel, shell, FUNC_SIMPLE_COPY, &shell->scrollCb);
    variable_make_function(default_callback_key, shell, FUNC_SIMPLE_COPY, &shell->keyCb);
    variable_make_function(default_callback_resize, shell, FUNC_SIMPLE_COPY, &shell->resizeCb);
    return builtin_ptr(shell, "mouseCb", &cbPtr, &shell->mouseCb, &shell->mouseCb)
        && builtin_ptr(shell, "scrollCb", &cbPtr, &shell->scrollCb, &shell->scrollCb)
        && builtin_ptr(shell, "keyCb", &cbPtr, &shell->keyCb, &shell->keyCb)
        && builtin_ptr(shell, "resizeCb", &cbPtr, &shell->resizeCb, &shell->resizeCb);
}
