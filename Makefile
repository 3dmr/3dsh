CFLAGS ?= -std=c89 -pedantic -march=native -Wall -Werror -O3 -D_POSIX_C_SOURCE=200112L
PREFIX ?= $(HOME)/.local
INCLUDEDIR ?= include
LIBDIR ?= lib
BINDIR ?= bin

NAME := 3dsh
DEMO_API_VERSION := 4
DEPS := "3dmr >= 0.1-127"
CFLAGS += $(shell pkg-config --cflags $(DEPS)) -DDEMO_API_VERSION=$(DEMO_API_VERSION) -DDEMO_PLUGIN_PATH=\"$(PREFIX)/$(LIBDIR)/$(NAME)\"
LDFLAGS += $(shell pkg-config --libs-only-L --libs-only-other $(DEPS))
WL_WHOLE_ARCHIVE := -Wl,--whole-archive
WL_NO_WHOLE_ARCHIVE := -Wl,--no-whole-archive
LDLIBS += -rdynamic -lm -ldl $(subst -l3dmr,$(WL_WHOLE_ARCHIVE) -l3dmr $(WL_NO_WHOLE_ARCHIVE),$(shell pkg-config --libs-only-l $(DEPS))) -lpthread

SOURCES := $(wildcard *.c)

PKG_CONFIG_CHECK := $(shell pkg-config --print-errors --short-errors --errors-to-stdout --exists $(DEPS) | sed "s/No package '\([^']*\)' found/\1/")
ifneq ($(PKG_CONFIG_CHECK),)
$(error Missing packages: $(PKG_CONFIG_CHECK))
endif

.PHONY: all
all: $(NAME)

$(NAME): $(SOURCES:.c=.o)

.PHONY: clean
clean:
	rm -f $(wildcard $(NAME) $(SOURCES:.c=.o) tags)

tags: $(SOURCES) $(wildcard *.h)
	ctags $^

compile_flags.txt: Makefile
	printf '%s\n' $(CFLAGS) > $@

ifneq ($(wildcard /usr/include/readline/readline.h),)
3dsh.o input.o completion.o: CFLAGS+=-DREADLINE
$(NAME): LDLIBS+=-lreadline
endif

.PHONY: install
D := $(if $(DESTDIR),$(DESTDIR)/)$(PREFIX)
install: $(NAME)
	mkdir -p $(D)/$(INCLUDEDIR)/3dsh $(D)/$(LIBDIR)/pkgconfig $(D)/$(BINDIR)
	cp $(NAME) $(D)/$(BINDIR)
	cp *.h $(D)/$(INCLUDEDIR)/3dsh/
	printf 'Name: 3dsh\nDescription: 3dsh\nVersion: $(DEMO_API_VERSION)\nRequires: %s\nCflags: -I%s -DDEMO_API_VERSION=$(DEMO_API_VERSION)\n' $(DEPS) '$(PREFIX)/$(INCLUDEDIR)' > $(D)/$(LIBDIR)/pkgconfig/3dsh.pc
