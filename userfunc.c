#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "eval.h"
#include "shell.h"
#include "parser.h"
#include "variable.h"
#include "userfunc.h"

int _eval_push_node(struct EvalState*, const struct AstNode*);

static int retval_get(const void* src, struct Variable* dest) {
    return 0;
}

static int retval_set(const struct Variable* src, void* dest) {
    return 0;
}

static void retval_free(struct Variable* v) {
    struct UserFunc* userfunc = v->data.dpointer.gdata;
    shell_pop_context(userfunc->evalState->shell);
}

static const struct PointerInfo retVal = {retval_get, retval_set, NULL, retval_free};

int function_ret(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    int ok = variable_copy(b, dest);
    if (!ok) {
        if ((*err = malloc(64))) {
            strcpy(*err, "failed to copy return value");
        }
    }
    return ok;
}

static int userfunc_exec(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct ShellRefCount* r = data;
    struct UserFunc* userfunc = r->data;
    struct AstNode retNode;
    unsigned int i;
    int ok;

    if (nArgs != userfunc->nArgs) {
        fprintf(stderr, "Error: function expected %u arguments but was called with %u\n", userfunc->nArgs, nArgs);
        return 0;
    }
    if (!shell_push_context(r->shell)) {
        fprintf(stderr, "Error: failed to push new shell context\n");
        return 0;
    }
    ok = 1;
    for (i = 0; ok && i < nArgs; i++) {
        struct Variable* tmp;
        ok = (tmp = shell_add_variable(r->shell, userfunc->argnames[i])) && variable_copy(args + i, tmp);
    }
    if (userfunc->evalState) {
        variable_make_ptr(&retVal, userfunc, ret, ret);
        retNode.type = BINARY_OP;
        retNode.data.binaryOp.a = NULL;
        retNode.data.binaryOp.b = NULL;
        retNode.data.binaryOp.op = function_ret;
        ok = ok && _eval_push_node(userfunc->evalState, &retNode)
                && _eval_push_node(userfunc->evalState, userfunc->body)
                && userfunc->evalState->numOps >= 3;
        if (ok) {
            struct AstNode** ops = userfunc->evalState->operators;
            unsigned int n = userfunc->evalState->numOps;
            struct AstNode* tmp = ops[n - 3];
            ops[n - 3] = ops[n - 2];
            ops[n - 2] = ops[n - 1];
            ops[n - 1] = tmp;
        } else {
            shell_pop_context(r->shell);
        }
    } else {
        struct EvalState evalState;
        if (ok && (ok = eval_node_start(userfunc->body, userfunc->shell, &evalState))) {
            while (eval_node_iter(&evalState));
            ok = eval_node_end(&evalState, ret);
        }
        shell_pop_context(r->shell);
    }

    return ok;
}

static void userfunc_destroy(struct DemoShell* shell, void* data) {
    struct UserFunc* userfunc = data;
    unsigned int i;
    if (!data) return;
    ast_free(userfunc->body);
    for (i = 0; i < userfunc->nArgs; i++) {
        free(userfunc->argnames[i]);
    }
    free(userfunc->argnames);
    free(userfunc);
}

int userfunc_make(struct DemoShell* shell, struct AstNode* decl, struct Variable* dest) {
    struct ShellRefCount* r;
    struct UserFunc* userfunc;
    unsigned int nArgs;

    if (decl->type != FUNCTION_DECL || (decl->data.functionDecl.arglist && decl->data.functionDecl.arglist->type != IDENTIFIER && decl->data.functionDecl.arglist->type != TUPLE)) {
        return 0;
    }
    if (!(r = shell_new_refcount(shell, userfunc_destroy, NULL))) {
        return 0;
    }
    if (!(userfunc = malloc(sizeof(*userfunc)))) {
        shell_decref(r);
        return 0;
    }
    if (!decl->data.functionDecl.arglist) {
        nArgs = 0;
    } else if (decl->data.functionDecl.arglist->type == IDENTIFIER) {
        nArgs = 1;
    } else {
        nArgs = decl->data.functionDecl.arglist->data.tuple.count;
    }
    r->data = userfunc;
    userfunc->evalState = NULL;
    userfunc->shell = shell;
    userfunc->body = NULL;
    userfunc->nArgs = 0;
    if (!(userfunc->argnames = malloc(nArgs * sizeof(char*)))) {
        shell_decref(r);
        return 0;
    }
    for (userfunc->nArgs = 0; userfunc->nArgs < nArgs; userfunc->nArgs++) {
        struct AstNode* argname = (decl->data.functionDecl.arglist->type == IDENTIFIER)
            ? decl->data.functionDecl.arglist
            : decl->data.functionDecl.arglist->data.tuple.list[userfunc->nArgs];
        if (argname->type != IDENTIFIER || !(userfunc->argnames[userfunc->nArgs] = malloc(strlen(argname->data.identifier.name) + 1))) {
            shell_decref(r);
            return 0;
        }
        strcpy(userfunc->argnames[userfunc->nArgs], argname->data.identifier.name);
    }
    if (!(userfunc->body = ast_deep_copy(decl->data.functionDecl.body))) {
        shell_decref(r);
        return 0;
    }
    variable_make_function(userfunc_exec, r, FUNC_USER | FUNC_DATAREF, dest);
    return 1;
}
