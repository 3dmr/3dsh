#include "shell.h"

#ifndef TDSH_TEXTURE_H
#define TDSH_TEXTURE_H

struct TextureData {
    char* description;
    unsigned int w, h, c, a;
    GLuint tex;
    GLint interp;
};

int shell_make_texture_vars(struct DemoShell* shell);

int variable_is_texture(const struct Variable* var);
struct TextureData* variable_to_texture(const struct Variable* var);
struct ShellRefCount* variable_to_texture_ref(const struct Variable* var);
int variable_make_texture(struct DemoShell* shell, GLuint tex, const char* description, struct Variable* dest);

#endif
