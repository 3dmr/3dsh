#include <stdio.h>
#include <pthread.h>
#include "builtins.h"
#include "eval.h"
#include "input.h"
#include "parser.h"
#include "render.h"
#include "shell.h"

int shell_main_loop(struct DemoShell* shell) {
    struct Parser parser;
    struct EvalState evalState;
    enum EvalStatus {EVAL_PARSE_INIT, EVAL_PARSE, EVAL_START, EVAL_LOOP, EVAL_END} status = EVAL_PARSE_INIT;
    struct Variable result;
    struct AstNode* ast;
    double dt, t = 0.0;
    unsigned long frames = 0;
    int running = 1, interactive = 0, render;
    pthread_t inputThread, renderThread;

    shell->curLine = "";
    if (pthread_create(&renderThread, NULL, shell_frame_swap_thread, shell)) {
        return 0;
    }
    if (pthread_create(&inputThread, NULL, shell_input_thread, shell)) {
        pthread_cancel(renderThread);
        return 0;
    }
    do {
        viewer_process_events(shell->viewer);
        if (shell->enableRender && !pthread_mutex_trylock(&shell->swapMutex)) {
            if ((render = !shell->renderDone)) {
                dt = shell->dt;
                viewer_make_current(shell->viewer);
                render_frame(shell);
                viewer_make_current(NULL);
                shell->renderDone = 1;
                pthread_cond_signal(&shell->swapCond);
            }
            pthread_mutex_unlock(&shell->swapMutex);
            if (render) {
                frames++;
                if ((t += dt) > 1.0) {
                    shell->fps = ((double)frames / t);
                    t = 0.0;
                    frames = 0;
                }
                shell_update_colors(&shell->colors, dt);
            }
        }
        if (shell->enableExec) {
            switch (status) {
                case EVAL_PARSE_INIT:
                    parser_init(&parser);
                    status = EVAL_PARSE;
                    if (pthread_mutex_lock(&shell->inputMutex)) {
                        break;
                    }
                    shell->prompt = shell->ps1;
                    shell->curLine = NULL;
                    pthread_cond_signal(&shell->inputCond);
                    pthread_mutex_unlock(&shell->inputMutex);
                    break;
                case EVAL_PARSE:
                    if (pthread_mutex_lock(&shell->inputMutex)) {
                        break;
                    }
                    if (shell->curLine) {
                        parse(&parser, shell->curLine);
                        if (parser.status == PARSE_ERROR) {
                            fprintf(stderr, "Syntax error: %s\n", parser.error);
                            parser_free(&parser);
                            status = EVAL_PARSE_INIT;
                        } else if (parse_empty(&parser)) {
                            parser_free(&parser);
                            status = EVAL_PARSE_INIT;
                        } else if (parse_need_more(&parser)) {
                            shell->prompt = shell->ps2;
                            shell->curLine = NULL;
                            pthread_cond_signal(&shell->inputCond);
                        } else {
                            status = EVAL_START;
                            interactive = shell->interactive;
                        }
                    } else {
                        struct timespec t;
                        t.tv_nsec = 100000000;
                        t.tv_sec = 0;
                        pthread_cond_timedwait(&shell->mainCond, &shell->inputMutex, &t);
                    }
                    pthread_mutex_unlock(&shell->inputMutex);
                    break;
                case EVAL_START:
                    if (!(ast = parse_end(&parser))) {
                        fprintf(stderr, "Syntax error: %s\n", parser.error);
                        parser_free(&parser);
                        status = EVAL_PARSE_INIT;
                    } else if (!eval_node_start(ast, shell, &evalState)) {
                        parser_free(&parser);
                        status = EVAL_PARSE_INIT;
                    } else {
                        status = EVAL_LOOP;
                    }
                    break;
                case EVAL_LOOP:
                    if (!eval_node_iter(&evalState)) {
                        status = EVAL_END;
                    }
                    break;
                case EVAL_END:
                    shell->lasterr = !eval_node_end(&evalState, &result);
                    if (!shell->lasterr) {
                        if (interactive) variable_print_line(&result);
                        variable_free(&result);
                    }
                    parser_free(&parser);
                    status = EVAL_PARSE_INIT;
                    break;
            }
        } else {
#ifdef _POSIX_C_SOURCE
            struct timespec t;
            t.tv_nsec = 100000000;
            t.tv_sec = 0;
            nanosleep(&t, NULL);
#endif
        }
        if (!pthread_mutex_lock(&shell->mainMutex)) {
            running = shell->running;
            pthread_mutex_unlock(&shell->mainMutex);
        }
    } while (running);
    if (status != EVAL_PARSE_INIT) parser_free(&parser);
    if (status == EVAL_LOOP || status == EVAL_END) {
        evalState.ok = 0;
        eval_node_end(&evalState, &result);
    }
    pthread_cancel(inputThread);
    pthread_cond_signal(&shell->swapCond);
    pthread_join(inputThread, NULL);
    pthread_join(renderThread, NULL);
    return 1;
}
