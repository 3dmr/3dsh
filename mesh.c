#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <3dmr/mesh/mesh.h>
#include <3dmr/mesh/box.h>
#include <3dmr/mesh/circle.h>
#include <3dmr/mesh/cone.h>
#include <3dmr/mesh/cylinder.h>
#include <3dmr/mesh/donut.h>
#include <3dmr/mesh/icosphere.h>
#include <3dmr/mesh/obj.h>
#include <3dmr/mesh/quad.h>
#include <3dmr/mesh/uvsphere.h>
#include "builtins.h"
#include "mesh.h"
#include "shell.h"
#include "vertex_array.h"

static void mesh_destroy_(struct DemoShell* shell, void* d) {
    if (!d) return;
    mesh_free(d);
    free(d);
}

static struct ShellRefCount* shell_new_mesh(struct DemoShell* shell) {
    struct ShellRefCount* r;
    struct Mesh* m = NULL;

    if (!(r = shell_new_refcount(shell, mesh_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return 0;
    }
    if (!(m = malloc(sizeof(*m)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return 0;
    }
    r->data = m;
    m->vertices = NULL;
    m->indices = NULL;
    m->numVertices = m->numIndices = m->flags = 0;
    return r;
}

#define MESH_MEMBER(name) \
static int mesh_##name(const void* src, struct Variable* dest) { \
    const struct Mesh* m = ((struct ShellRefCount*)src)->data; \
    variable_make_int(m->name, dest); \
    return 1; \
}

#define MESH_ATTR(name) \
static int mesh_##name(const void* src, struct Variable* dest) { \
    const struct Mesh* m = ((struct ShellRefCount*)src)->data; \
    variable_make_int(!!(m->flags & MESH_##name), dest); \
    return 1; \
}

MESH_MEMBER(numVertices)
MESH_MEMBER(numIndices)
MESH_ATTR(NORMALS)
MESH_ATTR(TEXCOORDS)
MESH_ATTR(TANGENTS)

static const struct FieldInfo structMeshFields[] = {
    {"numVertices", mesh_numVertices},
    {"numIndices", mesh_numIndices},
    {"hasNormals", mesh_NORMALS},
    {"hasTexcoords", mesh_TEXCOORDS},
    {"hasTangents", mesh_TANGENTS},
    {0}
};

static const struct StructInfo structMesh;

static int mesh_struct_copy(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(&structMesh, data, dest);
    return 1;
}

static void mesh_struct_free(void* data) {
    shell_decref(data);
}

static const struct StructInfo structMesh = {"Mesh", structMeshFields, mesh_struct_copy, mesh_struct_free};

static int quad(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec2 dim;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs == 1) {
        if (!variable_to_vec2(args, dim)) {
            fprintf(stderr, "Error: invalid param for quad()\n");
            return 0;
        }
    } else if (nArgs == 2) {
        unsigned int i;
        for (i = 0; i < 2; i++) {
            if (!variable_to_float(args + i, &dim[i])) {
                fprintf(stderr, "Error: invalid param %u for quad()\n", i);
                return 0;
            }
        }
    } else {
        fprintf(stderr, "Error: expected vec2 or 2 floats for quad()\n");
        return 0;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_quad(r->data, dim[0], dim[1])) {
        fprintf(stderr, "Error: failed to create quad mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int box(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    Vec3 dim;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs == 1) {
        if (!variable_to_vec3(args, dim)) {
            fprintf(stderr, "Error: invalid param for box()\n");
            return 0;
        }
    } else if (nArgs == 3) {
        unsigned int i;
        for (i = 0; i < 3; i++) {
            if (!variable_to_float(args + i, &dim[i])) {
                fprintf(stderr, "Error: invalid param %u for box()\n", i);
                return 0;
            }
        }
    } else {
        fprintf(stderr, "Error: expected vec3 or 3 floats for box()\n");
        return 0;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_box(r->data, dim[0], dim[1], dim[2])) {
        fprintf(stderr, "Error: failed to create box mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int cube(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    float dim;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs == 0) {
        dim = 1;
    } else if (nArgs == 1) {
        if (!variable_to_float(args, &dim)) {
            fprintf(stderr, "Error: invalid param for cube()\n");
            return 0;
        }
    } else {
        fprintf(stderr, "Error: expected a float argument for cube()\n");
        return 0;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_box(r->data, dim, dim, dim)) {
        fprintf(stderr, "Error: failed to create box mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int circle(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    float radius;
    long numVertices;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs == 2) {
        if (!variable_to_float(args, &radius) || radius < 0.0f) {
            fprintf(stderr, "Error: invalid radius param for circle()\n");
            return 0;
        }
        if (!variable_to_int(args + 1, &numVertices) || numVertices < 0 || numVertices > ((unsigned int)-1)) {
            fprintf(stderr, "Error: invalid numVertices param for circle()\n");
            return 0;
        }
    } else {
        fprintf(stderr, "Error: expected (float, int) in circle()\n");
        return 0;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_circle(r->data, radius, numVertices)) {
        fprintf(stderr, "Error: failed to create circle mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int cone(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    float depth, radius1, radius2 = 0;
    long numVertices = 32;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs < 2 || nArgs > 4) {
        fprintf(stderr, "Error: expected (float, float) in cone()\n");
        return 0;
    }
    if (!variable_to_float(args, &depth) || depth < 0.0f) {
        fprintf(stderr, "Error: invalid depth param for cone()\n");
        return 0;
    }
    if (!variable_to_float(args + 1, &radius1) || radius1 < 0.0f) {
        fprintf(stderr, "Error: invalid radius1 param for cone()\n");
        return 0;
    }
    if (nArgs >= 3) {
        if (!variable_to_int(args + 2, &numVertices) || numVertices < 0 || numVertices > ((unsigned int)-1)) {
            fprintf(stderr, "Error: invalid numVertices param for cone()\n");
            return 0;
        }
    }
    if (nArgs >= 4) {
        if (!variable_to_float(args + 3, &radius2) || radius2 < 0.0f) {
            fprintf(stderr, "Error: invalid radius2 param for cone()\n");
            return 0;
        }
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_cone_textured(r->data, radius1, radius2, depth, numVertices)) {
        fprintf(stderr, "Error: failed to create cone mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int cylinder(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    float depth, radius;
    long numVertices = 32;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs < 2 || nArgs > 3) {
        fprintf(stderr, "Error: expected (float, float) in cylinder()\n");
        return 0;
    }
    if (!variable_to_float(args, &depth) || depth < 0.0f) {
        fprintf(stderr, "Error: invalid depth param for cylinder()\n");
        return 0;
    }
    if (!variable_to_float(args + 1, &radius) || radius < 0.0f) {
        fprintf(stderr, "Error: invalid radius1 param for cylinder()\n");
        return 0;
    }
    if (nArgs >= 3) {
        if (!variable_to_int(args + 2, &numVertices) || numVertices < 0 || numVertices > ((unsigned int)-1)) {
            fprintf(stderr, "Error: invalid numVertices param for cylinder()\n");
            return 0;
        }
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_cylinder_textured(r->data, radius, depth, numVertices)) {
        fprintf(stderr, "Error: failed to create cone mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int donut(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    float majRadius, minRadius;
    long majNumPoints = 48, minNumPoints = 12, textured = 1;
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs == 2 || nArgs == 4 || nArgs == 5) {
        if (!variable_to_float(args, &majRadius) || majRadius < 0.0f
         || !variable_to_float(args + 1, &minRadius) || minRadius < 0.0f
         || (nArgs >= 4 && (
            !variable_to_int(args + 2, &majNumPoints) || majNumPoints < 0 || majNumPoints > ((unsigned int)-1)
         || !variable_to_int(args + 3, &minNumPoints) || minNumPoints < 0 || minNumPoints > ((unsigned int)-1)))
         || (nArgs == 5 && (
            !variable_to_int(args + 4, &textured)))) {
            fprintf(stderr, "Error: invalid param for donut()\n");
            return 0;
        }
    } else {
        fprintf(stderr, "Error: expected (float, float[, int, int[, int]]) in donut()\n");
        return 0;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!(textured ? make_donut_textured : make_donut)(r->data, majRadius, minRadius, majNumPoints, minNumPoints)) {
        fprintf(stderr, "Error: failed to create circle mesh\n");
        shell_decref(r);
        return 0;
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int sphere_args(const struct Variable* args, unsigned int nArgs, float* radius, unsigned int skip, enum SphereMapType* uvType, Vec3 uvParams, const char* fname) {
    if (nArgs < 1 || nArgs == (2 + skip) || nArgs > (3 + skip)) {
        fprintf(stderr, "Error: invalid number of arguments for %s()\n", fname);
        return 0;
    }
    if (!variable_to_float(args, radius)) {
        fprintf(stderr, "Error: invalid radius argument for %s()\n", fname);
        return 0;
    }
    if (nArgs >= (3 + skip)) {
        char* t;
        if (!(t = variable_to_string(args + 1 + skip))) {
            fprintf(stderr, "Error: invalid uvType argument for %s()\n", fname);
            return 0;
        }
        if (!strcmp(t, "cylindric")) {
            *uvType = SPHERE_MAP_CENTRAL_CYLINDRICAL;
        } else if (!strcmp(t, "mercator")) {
            *uvType = SPHERE_MAP_MERCATOR;
        } else if (!strcmp(t, "miller")) {
            *uvType = SPHERE_MAP_MILLER;
        } else if (!strcmp(t, "equirect")) {
            *uvType = SPHERE_MAP_EQUIRECTANGULAR;
        } else {
            fprintf(stderr, "Error: invalid uvType argument for %s()\n", fname);
            free(t);
            return 0;
        }
        free(t);
        if (!variable_to_vec3(args + 2 + skip, uvParams)) {
            fprintf(stderr, "Error: invalid uvParams argument for %s()\n", fname);
            return 0;
        }
    }
    return 1;
}

static int icosphere(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    Vec3 uvParams;
    float radius;
    unsigned int subdiv = 4;
    enum SphereMapType uvType = NUM_SPHERE_MAP;

    if (!sphere_args(args, nArgs, &radius, 1, &uvType, uvParams, "icosphere")) {
        return 0;
    }
    if (nArgs >= 2) {
        long tmp;
        if (!variable_to_int(args + 1, &tmp) || tmp <= 0 || tmp > ((long)((unsigned int)-1))) {
            fprintf(stderr, "Error: invalid subdiv argument for icosphere()\n");
            return 0;
        }
        subdiv = tmp;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_icosphere(r->data, radius, subdiv)) {
        fprintf(stderr, "Error: failed to create icosphere mesh\n");
        shell_decref(r);
        return 0;
    }
    if (uvType < NUM_SPHERE_MAP) {
        if (!compute_sphere_uv(r->data, uvParams[0], uvParams[1], uvParams[0], uvType)) {
            fprintf(stderr, "Error: failed to compute icosphere UVs\n");
            shell_decref(r);
            return 0;
        }
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int uvsphere(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    Vec3 uvParams;
    float radius;
    long tmp;
    unsigned int latNum = 16, longNum = 32;
    enum SphereMapType uvType = NUM_SPHERE_MAP;

    if (!sphere_args(args, nArgs, &radius, 2, &uvType, uvParams, "uvsphere")) {
        return 0;
    }
    if (nArgs >= 2) {
        if (!variable_to_int(args + 1, &tmp) || tmp <= 0 || tmp > ((long)((unsigned int)-1))) {
            fprintf(stderr, "Error: invalid latNum argument for uvsphere()\n");
            return 0;
        }
        latNum = tmp;
    }
    if (nArgs >= 2) {
        if (!variable_to_int(args + 2, &tmp) || tmp <= 0 || tmp > ((long)((unsigned int)-1))) {
            fprintf(stderr, "Error: invalid longNum argument for uvsphere()\n");
            return 0;
        }
        longNum = tmp;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_uvsphere(r->data, radius, longNum, latNum)) {
        fprintf(stderr, "Error: failed to create uvsphere mesh\n");
        shell_decref(r);
        return 0;
    }
    if (uvType < NUM_SPHERE_MAP) {
        if (!compute_sphere_uv(r->data, uvParams[0], uvParams[1], uvParams[0], uvType)) {
            fprintf(stderr, "Error: failed to compute icosphere UVs\n");
            shell_decref(r);
            return 0;
        }
    }
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int mesh_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    char* path;

    if (nArgs != 1 || !(path = variable_to_string(args))) {
        fprintf(stderr, "Error: expected a string argument for mesh()\n");
        return 0;
    }
    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    if (!make_obj(r->data, path, 0, 1, 1)) {
        fprintf(stderr, "Error: failed to load mesh from obj '%s'\n", path);
        free(path);
        shell_decref(r);
        return 0;
    }
    free(path);
    variable_make_struct(&structMesh, r, ret);
    return 1;
}

static int compute_tangents(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    int ptr, r = 0;

    if (nArgs != 1) {
        fprintf(stderr, "Error: compute_tangents() needs one argument\n");
        return 0;
    }
    ptr = variable_move_dereference(args, ret);
    if (ret->type != STRUCT || ret->data.dstruct.info != &structMesh) {
        fprintf(stderr, "Error: compute_tangents() needs one mesh argument\n");
    } else if (!mesh_compute_tangents(((struct ShellRefCount*)ret->data.dstruct.data)->data)) {
        fprintf(stderr, "Error: failed to compute tangents\n");
    } else {
        r = 1;
    }
    if (ptr) variable_free(ret);
    ret->type = VOID;
    return r;
}

static int export_obj(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    FILE* f = NULL;
    char* path = NULL;
    int ptr, r = 0;

    if (nArgs != 2) {
        fprintf(stderr, "Error: export_obj() needs two arguments\n");
        return 0;
    }
    ptr = variable_move_dereference(args, ret);
    if (ret->type != STRUCT || ret->data.dstruct.info != &structMesh) {
        fprintf(stderr, "Error: export_obj() needs a mesh argument\n");
    } else if (!(path = variable_to_string(args + 1))) {
        fprintf(stderr, "Error: export_obj() needs a string path argument\n");
    } else if (!(f = fopen(path, "w"))) {
        fprintf(stderr, "Error: failed to open '%s'\n", path);
    } else if (!mesh_save_obj(((struct ShellRefCount*)ret->data.dstruct.data)->data, f)) {
        fprintf(stderr, "Error: failed to export obj\n");
    } else {
        r = 1;
    }
    if (ptr) variable_free(ret);
    if (f) fclose(f);
    free(path);
    ret->type = VOID;
    return r;
}

struct VAttrData {
    struct ShellRefCount* mesh;
    unsigned int f, i;
};

#define MESH_BITANGENT 3

static int mesh_vattr_check(const struct Mesh* mesh, unsigned int f, unsigned int i) {
    switch (f) {
        case 0: break;
        case MESH_NORMALS: if (!MESH_HAS_NORMALS(mesh)) return 0; break;
        case MESH_TEXCOORDS: if (!MESH_HAS_TEXCOORDS(mesh)) return 0; break;
        case MESH_TANGENTS: if (!MESH_HAS_TANGENTS(mesh)) return 0; break;
        case MESH_BITANGENT: if (!MESH_HAS_TANGENTS(mesh)) return 0; break;
        default: return 0;
    }
    return (i < mesh->numVertices);
}

static int mesh_vattrptr_check(const struct Mesh* mesh, unsigned int f, unsigned int i, unsigned int* o, unsigned int* n) {
    *n = 3;
    switch (f) {
        case 0: *o = 0; break;
        case MESH_NORMALS: if (!MESH_HAS_NORMALS(mesh)) return 0; *o = 3; break;
        case MESH_TEXCOORDS: *n = 2; if (!MESH_HAS_TEXCOORDS(mesh)) return 0; *o = 3 * (1 + MESH_HAS_NORMALS(mesh)); break;
        case MESH_TANGENTS: if (!MESH_HAS_TANGENTS(mesh)) return 0; *o = 3 * (1 + MESH_HAS_NORMALS(mesh)) + 2 * MESH_HAS_TEXCOORDS(mesh); break;
        case MESH_BITANGENT: if (!MESH_HAS_TANGENTS(mesh)) return 0; *o = 3 * (2 + MESH_HAS_NORMALS(mesh)) + 2 * MESH_HAS_TEXCOORDS(mesh); break;
        default: return 0;
    }
    return (i < mesh->numVertices);
}

static int mesh_vattr_get(const void* src, struct Variable* dest) {
    const struct VAttrData* vd = ((const struct ShellRefCount*)src)->data;
    const struct Mesh* mesh = vd->mesh->data;
    unsigned int o, n;
    if (!mesh_vattrptr_check(mesh, vd->f, vd->i, &o, &n)) return 0;
    memcpy(&dest->data, mesh->vertices + MESH_FLOATS_PER_VERTEX(mesh) * vd->i + o, n * sizeof(float));
    dest->type = VEC2 + (n - 2);
    return 1;
}

static int mesh_vattr_set(const struct Variable* src, void* dest) {
    const struct VAttrData* vd = ((const struct ShellRefCount*)dest)->data;
    const struct Mesh* mesh = vd->mesh->data;
    unsigned int o, n;
    if (!mesh_vattrptr_check(mesh, vd->f, vd->i, &o, &n) || src->type != (VEC2 + (n - 2))) return 0;
    memcpy(mesh->vertices + MESH_FLOATS_PER_VERTEX(mesh) * vd->i + o, &src->data, n * sizeof(float));
    return 1;
}

static int mesh_vattr_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.gdata)) return 0;
    *dest = *src;
    return 1;
}

void mesh_vattr_free(struct Variable* v) {
    shell_decref(v->data.dpointer.gdata);
}

void vattr_destroy(struct DemoShell* shell, void* data) {
    if (!data) return;
    shell_decref(((struct VAttrData*)data)->mesh);
    free(data);
}

static const struct PointerInfo ptrVAttr = {mesh_vattr_get, mesh_vattr_set, mesh_vattr_copy, mesh_vattr_free};

static int mesh_vattr(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret, const char* fname, unsigned int flag) {
    struct DemoShell* shell = data;
    struct VAttrData* vd = NULL;
    struct ShellRefCount* vr = NULL;
    long i;
    int r = 0;

    if (nArgs != 2 || !variable_to_int(args + 1, &i) || i < 0 || i > ((unsigned int)-1)) {
        fprintf(stderr, "Error: %s() needs two arguments (Mesh, int)\n", fname);
        return 0;
    }
    if (!variable_copy_dereference(args, ret)) {
        fprintf(stderr, "Error: failed to copy mesh\n");
        return 0;
    }
    if (ret->type != STRUCT || ret->data.dstruct.info != &structMesh) {
        fprintf(stderr, "Error: %s() needs two arguments (Mesh, int)\n", fname);
    } else if (!mesh_vattr_check(((struct ShellRefCount*)ret->data.dstruct.data)->data, flag, i)) {
        fprintf(stderr, "Error: unsupported mesh operation (bad index or attribute)\n");
    } else if (!(vr = shell_new_refcount(shell, vattr_destroy, NULL)) || !(vd = malloc(sizeof(*vd)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
    } else {
        r = 1;
    }
    if (r) {
        vr->data = vd;
        vd->f = flag;
        vd->i = i;
        vd->mesh = ret->data.dstruct.data;
        variable_make_ptr(&ptrVAttr, vr, vr, ret);
    } else {
        variable_free(ret);
        free(vd);
        if (vr) shell_decref(vr);
    }
    return r;
}

static int vertex(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    return mesh_vattr(data, args, nArgs, ret, "vertex", 0);
}

static int normal(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    return mesh_vattr(data, args, nArgs, ret, "normal", MESH_NORMALS);
}

static int texcoord(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    return mesh_vattr(data, args, nArgs, ret, "texcoord", MESH_TEXCOORDS);
}

static int tangent(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    return mesh_vattr(data, args, nArgs, ret, "tangent", MESH_TANGENTS);
}

static int bitangent(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    return mesh_vattr(data, args, nArgs, ret, "bitangent", MESH_BITANGENT);
}

int shell_make_mesh_functions(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "quad" , quad, shell)
        && builtin_function_with_data(shell, "box" , box, shell)
        && builtin_function_with_data(shell, "cube" , cube, shell)
        && builtin_function_with_data(shell, "circle" , circle, shell)
        && builtin_function_with_data(shell, "cone" , cone, shell)
        && builtin_function_with_data(shell, "cylinder" , cylinder, shell)
        && builtin_function_with_data(shell, "donut" , donut, shell)
        && builtin_function_with_data(shell, "icosphere" , icosphere, shell)
        && builtin_function_with_data(shell, "uvsphere" , uvsphere, shell)
        && builtin_function_with_data(shell, "mesh" , mesh_, shell)
        && builtin_function(shell, "compute_tangents" , compute_tangents)
        && builtin_function_with_data(shell, "vertex" , vertex, shell)
        && builtin_function_with_data(shell, "normal" , normal, shell)
        && builtin_function_with_data(shell, "texcoord" , texcoord, shell)
        && builtin_function_with_data(shell, "tangent" , tangent, shell)
        && builtin_function_with_data(shell, "bitangent" , bitangent, shell)
        && builtin_function(shell, "export_obj" , export_obj);
}

int variable_is_mesh(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structMesh;
}

struct Mesh* variable_to_mesh(struct Variable* var) {
    if (variable_is_mesh(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}

int variable_make_mesh(struct DemoShell* shell, struct Variable* ret) {
    struct ShellRefCount* r;
    struct Mesh* mesh;

    if (!(r = shell_new_mesh(shell))) {
        return 0;
    }
    mesh = r->data;
    mesh->vertices = NULL;
    mesh->indices = NULL;
    mesh->numVertices = 0;
    mesh->numIndices = 0;
    mesh->flags = 0;
    variable_make_struct(&structMesh, r, ret);
    return 1;
}
