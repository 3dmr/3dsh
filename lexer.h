#ifndef TDSH_LEXER_H
#define TDSH_LEXER_H

union Token {
    float f;
    long i;
    char* s;
};

int next_token(const char* s, union Token* token, char** end);

#endif
