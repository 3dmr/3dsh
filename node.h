#include "shell.h"

#ifndef TDSH_NODE_H
#define TDSH_NODE_H

struct NodeVarData {
    union NodeTypeVarData {
        struct GeomNodeData {
            struct Variable material, vertexArray;
            struct Geometry geometry;
        } geom;
        struct Variable light;
        struct Variable camera;
    } data;
    struct ShellRefCount* self;
};

int shell_make_node_functions(struct DemoShell* shell);

const struct StructInfo* node_struct_type(enum NodeType type);
int variable_is_node(const struct Variable* var);
struct Node* variable_to_node(const struct Variable* var);
int variable_is_scene(const struct Variable* var);
struct Node* variable_to_scene(const struct Variable* var);
struct NodeVarData* node_get_var_data(const struct Node* node);
int variable_make_node(struct DemoShell* shell, struct Node* node, struct Variable* dest);

#endif
