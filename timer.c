#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "timer.h"
#include "builtins.h"

static int timer_dt(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct ShellRefCount* r = data;
    double* timer = r->data;
    double current = *timer;
    variable_make_float((*timer = glfwGetTime()) - current, ret);
    return 1;
}

static void timer_destroy_(struct DemoShell* shell, void* data) {
    free(data);
}

static int timer_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct ShellRefCount* r;
    double* timer;
    if (!(timer = malloc(sizeof(*timer)))) {
        fprintf(stderr, "Error: failed to allocate timer\n");
        return 0;
    }
    if (!(r = shell_new_refcount(shell, timer_destroy_, timer))) {
        fprintf(stderr, "Error: failed to allocate timer\n");
        free(timer);
        return 0;
    }
    *timer = glfwGetTime();
    variable_make_function(timer_dt, r, FUNC_DATAREF, ret);
    return 1;
}

static int abstime_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    variable_make_int(time(NULL), ret);
    return 1;
}

static int reltime_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    variable_make_float(glfwGetTime(), ret);
    return 1;
}

int shell_make_timer_functions(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "timer", timer_, shell)
        && builtin_function(shell, "abstime", abstime_)
        && builtin_function(shell, "reltime", reltime_);
}
