#include <3dmr/mesh/mesh.h>
#include "shell.h"

#ifndef TDSH_VERTEX_ARRAY_H
#define TDSH_VERTEX_ARRAY_H

int vertex_array_from_mesh(struct DemoShell* shell, const struct Mesh* mesh, struct Variable* dest);
int variable_make_vertex_array(struct DemoShell* shell, struct VertexArray* vertexArray, struct Variable* dest);

int shell_make_vertex_array_functions(struct DemoShell* shell);

int variable_is_vertex_array(const struct Variable* var);
struct VertexArray* variable_to_vertex_array(struct Variable* var);

#endif
