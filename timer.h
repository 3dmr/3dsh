#include "shell.h"

#ifndef TDSH_TIMER_H
#define TDSH_TIMER_H

int shell_make_timer_functions(struct DemoShell* shell);

#endif
