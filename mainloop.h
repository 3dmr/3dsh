#include "shell.h"

#ifndef TDSH_MAINLOOP_H
#define TDSH_MAINLOOP_H

int shell_main_loop(struct DemoShell* shell);

#endif
