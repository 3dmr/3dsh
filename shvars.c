#include <string.h>
#include "builtins.h"
#include "shvars.h"

static int int_ptr_get(const void* src, struct Variable* dest) {
    variable_make_int(*(unsigned int*)src, dest);
    return 1;
}

#define viewer_var(name) \
static int viewer_##name##_get(const void* src, struct Variable* dest) { \
    const struct DemoShell* shell = src; \
    variable_make_int(shell->viewer->name, dest); \
    return 1; \
} \
static const struct PointerInfo viewer##name##Ptr = {viewer_##name##_get, NULL, NULL, NULL};
viewer_var(width)
viewer_var(height)

static const struct PointerInfo stringPtr = {string_ptr_get, string_ptr_set, NULL, NULL};
static const struct PointerInfo floatPtr = {float_ptr_get, NULL, NULL, NULL};
static const struct PointerInfo intPtr = {int_ptr_get, NULL, NULL, NULL};

static int argv_get(const void* src, unsigned int i, struct Variable* dest) {
    const struct ShellConfig* config = src;
    return i < config->argc && variable_make_string(config->argv[i], strlen(config->argv[i]), dest);
}

static const struct ArrayInfo argv = {argv_get, 0, 0, 0, 0};

int shell_make_shvars(struct DemoShell* shell) {
    struct Variable* dest;
    if (!(dest = shell_add_variable(shell, "argv"))) {
        return 0;
    }
    variable_make_array(&argv, &shell->config, &shell->config.argc, dest);
    return builtin_ptr(shell, "ps1", &stringPtr, &shell->ps1, &shell->ps1)
        && builtin_ptr(shell, "ps2", &stringPtr, &shell->ps2, &shell->ps2)
        && builtin_ptr(shell, "fps", &floatPtr, &shell->fps, NULL)
        && builtin_ptr(shell, "numRenderedObjs", &intPtr, &shell->scene.nRender, NULL)
        && builtin_ptr(shell, "width", &viewerwidthPtr, shell, NULL)
        && builtin_ptr(shell, "height", &viewerheightPtr, shell, NULL);
}
