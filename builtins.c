#include "shell.h"
#include "builtins.h"
#include "camera.h"
#include "constants.h"
#include "lights.h"
#include "shvars.h"
#include "math.h"
#include "mesh.h"
#include "vertex_array.h"
#include "node.h"
#include "sysfuncs.h"
#include "colors.h"
#include "material.h"
#include "skybox.h"
#include "texture.h"
#include "renderfuncs.h"
#include "callbacks.h"
#include "timer.h"
#include "random.h"
#include "opengex.h"

int builtin_int(struct DemoShell* shell, const char* name, long v) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, name))) {
        return 0;
    }
    variable_make_int(v, dest);

    return 1;
}

int builtin_float(struct DemoShell* shell, const char* name, float v) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, name))) {
        return 0;
    }
    variable_make_float(v, dest);

    return 1;
}

int builtin_vec3(struct DemoShell* shell, const char* name, float x, float y, float z) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, name))) {
        return 0;
    }
    dest->type = VEC3;
    dest->data.dvec3[0] = x;
    dest->data.dvec3[1] = y;
    dest->data.dvec3[2] = z;

    return 1;
}

int builtin_ptr(struct DemoShell* shell, const char* name, const struct PointerInfo* info, void* gdata, void* sdata) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, name))) {
        return 0;
    }
    variable_make_ptr(info, gdata, sdata, dest);

    return 1;
}

int builtin_function(struct DemoShell* shell, const char* name, int (*f)(void*, const struct Variable*, unsigned int, struct Variable*)) {
    return builtin_function_with_data(shell, name, f, NULL);
}

int builtin_function_with_data(struct DemoShell* shell, const char* name, int (*f)(void*, const struct Variable*, unsigned int, struct Variable*), void* data) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, name))) {
        return 0;
    }
    variable_make_function(f, data, FUNC_SIMPLE_COPY, dest);

    return 1;
}

int builtin_struct(struct DemoShell* shell, const char* name, const struct StructInfo* sinfo, void* data) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, name))) {
        return 0;
    }
    variable_make_struct(sinfo, data, dest);

    return 1;
}

int shell_make_builtins(struct DemoShell* shell) {
    return shell_make_camera(shell)
        && shell_make_lights(shell)
        && shell_make_constants(shell)
        && shell_make_shvars(shell)
        && shell_make_math_functions(shell)
        && shell_make_sys_functions(shell)
        && shell_make_mesh_functions(shell)
        && shell_make_vertex_array_functions(shell)
        && shell_make_node_functions(shell)
        && shell_make_color_vars(shell)
        && shell_make_material_vars(shell)
        && shell_make_skybox_vars(shell)
        && shell_make_texture_vars(shell)
        && shell_make_render_functions(shell)
        && shell_make_callbacks(shell)
        && shell_make_timer_functions(shell)
        && shell_make_random_functions(shell)
        && shell_make_opengex_funcs(shell);
}
