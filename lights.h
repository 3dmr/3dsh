#include "shell.h"
#include "dlight.h"
#include "plight.h"
#include "slight.h"

#ifndef TDSH_LIGHTS_H
#define TDSH_LIGHTS_H

int shell_make_lights(struct DemoShell* shell);

#endif
