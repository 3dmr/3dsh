#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <limits.h>
#include <3dmr/material/solid.h>
#include <3dmr/material/phong.h>
#include <3dmr/material/pbr.h>
#include <3dmr/render/vertex_array.h>
#include "parser.h"
#include "variable.h"
#include "shell.h"
#include "userfunc.h"

#define NUM_TYPES (VOID + 1)
static const char* typeNames[NUM_TYPES] = {"int", "float", "vec2", "vec3", "vec4", "mat2", "mat3", "mat4", "quaternion", "string", "pointer", "array", "struct", "function", "void"};

static void newline(unsigned int indent) {
    fputc('\n', stdout);
    while (indent--) {
        fputc(' ', stdout);
    }
}

const char* variable_type_string(enum VarType type) {
    if (type >= 0 && type < NUM_TYPES) {
        return typeNames[type];
    }
    return NULL;
}

void variable_print(const struct Variable* var, unsigned int indent, int escape) {
    switch (var->type) {
        case INT:
            printf("%ld", var->data.dint);
            break;
        case FLOAT:
            printf("%g", var->data.dfloat);
            break;
        case VEC2:
            printf("(%g, %g)", var->data.dvec2[0], var->data.dvec2[1]);
            break;
        case VEC3:
            printf("(%g, %g, %g)", var->data.dvec3[0], var->data.dvec3[1], var->data.dvec3[2]);
            break;
        case VEC4:
            printf("(%g, %g, %g, %g)", var->data.dvec4[0], var->data.dvec4[1], var->data.dvec4[2], var->data.dvec4[3]);
            break;
        case MAT2:
            fputc('(', stdout);
            newline(indent += 4);
            printf("%g, %g,", var->data.dmat2[0][0], var->data.dmat2[1][0]);
            newline(indent);
            printf("%g, %g", var->data.dmat2[0][1], var->data.dmat2[1][1]);
            newline(indent -= 4);
            fputc(')', stdout);
            break;
        case MAT3:
            fputc('(', stdout);
            newline(indent += 4);
            printf("%g, %g, %g,", var->data.dmat3[0][0], var->data.dmat3[1][0], var->data.dmat3[2][0]);
            newline(indent);
            printf("%g, %g, %g,", var->data.dmat3[0][1], var->data.dmat3[1][1], var->data.dmat3[2][1]);
            newline(indent);
            printf("%g, %g, %g", var->data.dmat3[0][2], var->data.dmat3[1][2], var->data.dmat3[2][2]);
            newline(indent -= 4);
            fputc(')', stdout);
            break;
        case MAT4:
            fputc('(', stdout);
            newline(indent += 4);
            printf("%g, %g, %g, %g,", var->data.dmat4[0][0], var->data.dmat4[1][0], var->data.dmat4[2][0], var->data.dmat4[3][0]);
            newline(indent);
            printf("%g, %g, %g, %g,", var->data.dmat4[0][1], var->data.dmat4[1][1], var->data.dmat4[2][1], var->data.dmat4[3][1]);
            newline(indent);
            printf("%g, %g, %g, %g,", var->data.dmat4[0][2], var->data.dmat4[1][2], var->data.dmat4[2][2], var->data.dmat4[3][2]);
            newline(indent);
            printf("%g, %g, %g, %g",  var->data.dmat4[0][3], var->data.dmat4[1][3], var->data.dmat4[2][3], var->data.dmat4[3][3]);
            newline(indent -= 4);
            fputc(')', stdout);
            break;
        case QUATERNION:
            {
                Vec3 axis;
                float angle = quaternion_get_angle((float*)var->data.dquaternion);
                if (angle) {
                    quaternion_get_axis((float*)var->data.dquaternion, axis);
                    printf("{axis=(%g, %g, %g), angle=%g}", axis[0], axis[1], axis[2], angle);
                } else {
                    fputs("{axis=N/A, angle=0}", stdout);
                }
            }
            break;
        case STRING:
            if (escape) {
                unsigned char* ptr;
                fputc('"', stdout);
                for (ptr = (unsigned char*)var->data.dstring; *ptr; ptr++) {
                    if ((*ptr >= 32 && *ptr < 127) || *ptr >= 128) {
                        fputc(*ptr, stdout);
                    } else {
                        printf("\\x%02X", (int)(*ptr));
                    }
                }
                fputc('"', stdout);
            } else {
                printf("%s", var->data.dstring);
            }
            break;
        case POINTER:
            {
                struct Variable tmp;
                if (variable_pointer_get(var, &tmp)) {
                    variable_print(&tmp, indent, escape);
                    variable_free(&tmp);
                } else {
                    printf("(failed to deref pointer)");
                }
            }
            break;
        case ARRAY:
            {
                struct Variable tmp;
                unsigned int i = 0;
                fputc('[', stdout);
                if (*var->data.darray.count) {
                    indent += 4;
                    goto array_start;
                    do {
                        fputc(',', stdout);
array_start:
                        newline(indent);
                        if (variable_array_get(var, i, &tmp)) {
                            variable_print(&tmp, indent, escape);
                            variable_free(&tmp);
                        } else {
                            printf("(failed to access member)");
                        }
                    } while (++i < *var->data.darray.count);
                    indent -= 4;
                    newline(indent);
                }
                fputc(']', stdout);
            }
            break;
        case STRUCT:
            {
                struct Variable tmp;
                const struct FieldInfo* field = var->data.dstruct.info->fields;
                fputc('{', stdout);
                if (field->name) {
                    indent += 4;
                    goto struct_start;
                    do {
                        fputc(',', stdout);
struct_start:
                        newline(indent);
                        printf("%s=", field->name);
                        if (variable_struct_get(var, field, &tmp)) {
                            variable_print(&tmp, indent, escape);
                            variable_free(&tmp);
                        } else {
                            printf("(failed to get member)");
                        }
                    } while ((++field)->name);
                    indent -= 4;
                    newline(indent);
                }
                fputc('}', stdout);
            }
            break;
        case FUNCTION:
            if (var->data.dfunction.flags & FUNC_USER) {
                fputs("user-defined function", stdout);
            } else {
                fputs("builtin function", stdout);
            }
            break;
        case VOID:
            break;
    }
}

void variable_print_line(const struct Variable* var) {
    struct Variable tmp;
    int pointer = variable_move_dereference(var, &tmp);

    if (tmp.type == VOID) return;
    if (tmp.type == STRUCT) {
        printf("struct %s: ", tmp.data.dstruct.info->name);
    } else {
        const char* type = variable_type_string(tmp.type);
        if (!type) {
            return;
        }
        printf("%s: ", type);
    }
    if (tmp.type == POINTER) {
        printf("failed to deref pointer");
    } else {
        variable_print(&tmp, 0, 1);
    }
    fputc('\n', stdout);
    if (pointer) variable_free(&tmp);
}

int variable_array_add(struct Variable* a, const struct Variable* src) {
    return a->data.darray.info->add(a->data.darray.data, src);
}

int variable_array_del(struct Variable* a, long i) {
    return a->data.darray.info->del(a->data.darray.data, i);
}

int variable_array_get(const struct Variable* a, long i, struct Variable* dest) {
    return a->data.darray.info->get(a->data.darray.data, i, dest);
}

int variable_struct_get(const struct Variable* s, const struct FieldInfo* field, struct Variable* dest) {
    return field->get(s->data.dstruct.data, dest);
}

int variable_pointer_get(const struct Variable* src, struct Variable* dest) {
    return src->data.dpointer.info->get(src->data.dpointer.gdata, dest);
}

int variable_pointer_set(const struct Variable* src, struct Variable* dest) {
    return dest->data.dpointer.info->set && dest->data.dpointer.info->set(src, dest->data.dpointer.sdata);
}

int variable_copy_dereference(const struct Variable* src, struct Variable* dest) {
    if (!variable_copy(src, dest)) {
        return 0;
    }
    while (dest->type == POINTER) {
        struct Variable tmp;
        if (!variable_pointer_get(dest, &tmp)) {
            variable_free(dest);
            return 0;
        }
        variable_free(dest);
        *dest = tmp;
    }
    return 1;
}

int variable_move_dereference(const struct Variable* src, struct Variable* dest) {
    int ret = 0;
    *dest = *src;
    while (dest->type == POINTER) {
        struct Variable tmp;
        if (!variable_pointer_get(dest, &tmp)) {
            if (ret) variable_free(dest);
            *dest = *src;
            return 0;
        }
        if (ret) variable_free(dest);
        *dest = tmp;
        ret = 1;
    }
    return ret;
}

int variable_function_call(const struct Variable* src, struct Variable* dest, const struct Variable* args, unsigned int nArgs, struct EvalState* es) {
    if (src->data.dfunction.flags & FUNC_USER) {
        struct ShellRefCount* r = src->data.dfunction.data;
        struct UserFunc* userfunc = r->data;
        userfunc->evalState = es;
    }
    return src->data.dfunction.f(src->data.dfunction.data, args, nArgs, dest);
}

int variable_function_call_v(const struct Variable* func, struct Variable* ret, const char* format, va_list args) {
    struct Variable* argv;
    const char* vs;
    long vl;
    unsigned int i, nArgs;
    unsigned int ok = 1, l, u, d, v, m, *f;

    for (i = nArgs = 0; format[i]; i++) {
        nArgs += (format[i] == '%');
    }
    if (!(argv = malloc(nArgs * sizeof(*argv)))) return 0;
    for (i = 0; i < nArgs; i++) {
        argv[i].type = VOID;
    }
    for (i = 0; ok && i < nArgs; i++) {
        l = u = d = v = m = 0;
        if (*format != '%') {
            ok = 0;
            break;
        }
        format++;
        while (ok && *format && *format != '%') {
            switch (*format) {
                case 'l': f = &l; goto flag;
                case 'v': f = &v; goto flag;
                case 'm': f = &m; goto flag;
                flag:
                    if (*f) {
                        ok = 0;
                    } else {
                        *f = 1;
                    }
                    ok = ok && !(v && m);
                    break;
                case 'u':
                    u = 1;
                case 'd':
                    if (u) {
                        unsigned long tmp;
                        tmp = l ? va_arg(args, unsigned long) : ((unsigned long)va_arg(args, unsigned int));
                        if ((ok = tmp < ((unsigned long)LONG_MAX))) {
                            vl = tmp;
                        }
                    } else {
                        vl = l ? va_arg(args, long) : ((long)va_arg(args, int));
                    }
                    if ((ok = ok && !(d | v | m))) variable_make_int(vl, argv + i);
                    d = 1;
                    break;
                case 'f':
                    if ((ok = ok && !(d | u | l | v | m))) variable_make_float(va_arg(args, double), argv + i);
                    d = 1;
                    break;
                case 's':
                    vs = va_arg(args, char*);
                    ok = ok && !(d | u | l | v | m) && vs && variable_make_string(vs, strlen(vs), argv + i);
                    d = 1;
                    break;
                case '2':
                case '3':
                case '4':
                    if ((ok = ok && !(d | u | l) && (v | m))) {
                        const void* src;
                        unsigned int nfloats;
                        if (v) {
                            nfloats = (*format - '0');
                            argv[i].type = VEC2 + (*format - '2');
                            src = va_arg(args, float*);
                        } else {
                            nfloats = (*format - '0') * (*format - '0');
                            argv[i].type = MAT2 + (*format - '2');
                            src = va_arg(args, float(*)[]);
                        }
                        memcpy(&argv[i].data, src, nfloats * sizeof(float));
                    }
                    d = 1;
                    break;
                case 'q':
                    if ((ok = ok && !(d | u | l | v | m))) {
                        argv[i].type = QUATERNION;
                        memcpy(argv[i].data.dquaternion, va_arg(args, float*), sizeof(Quaternion));
                    }
                    d = 1;
                    break;
                default:
                    ok = 0;
                    break;
            }
            format++;
        }
    }
    ok = ok && variable_function_call(func, ret, argv, nArgs, NULL);
    for (i = 0; i < nArgs; i++) {
        variable_free(argv + i);
    }
    free(argv);
    return ok;
}

int variable_function_call_l(const struct Variable* func, struct Variable* ret, const char* format, ...) {
    va_list args;
    int r;
    va_start(args, format);
    r = variable_function_call_v(func, ret, format, args);
    va_end(args);
    return r;
}

int variable_to_int(const struct Variable* var, long* dest) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   *dest = tmp.data.dint; return 1;
        case FLOAT: *dest = tmp.data.dfloat; return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_float(const struct Variable* var, float* dest) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   *dest = tmp.data.dint; return 1;
        case FLOAT: *dest = tmp.data.dfloat; return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

char* variable_to_string(const struct Variable* var) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return NULL;
    switch (tmp.type) {
        case STRING: return tmp.data.dstring;
        default:;
    }
    variable_free(&tmp);
    return NULL;
}

int variable_to_vec2(const struct Variable* var, Vec2 dest) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   dest[0] = dest[1] = tmp.data.dint; return 1;
        case FLOAT: dest[0] = dest[1] = tmp.data.dfloat; return 1;
        case VEC2:  memcpy(dest, tmp.data.dvec2, sizeof(Vec2)); return 1;
        case VEC3:  memcpy(dest, tmp.data.dvec3, sizeof(Vec2)); return 1;
        case VEC4:  memcpy(dest, tmp.data.dvec4, sizeof(Vec2)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_vec3(const struct Variable* var, Vec3 dest) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   dest[0] = dest[1] = dest[2] = tmp.data.dint; return 1;
        case FLOAT: dest[0] = dest[1] = dest[2] = tmp.data.dfloat; return 1;
        case VEC3:  memcpy(dest, tmp.data.dvec3, sizeof(Vec3)); return 1;
        case VEC4:  memcpy(dest, tmp.data.dvec4, sizeof(Vec3)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_vec4(const struct Variable* var, Vec4 dest) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:        dest[0] = dest[1] = dest[2] = dest[3] = tmp.data.dint; return 1;
        case FLOAT:      dest[0] = dest[1] = dest[2] = dest[3] = tmp.data.dfloat; return 1;
        case VEC4:       memcpy(dest, tmp.data.dvec4, sizeof(Vec4)); return 1;
        case QUATERNION: memcpy(dest, tmp.data.dquaternion, sizeof(Vec4)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_quaternion(const struct Variable* var, Quaternion dest) {
    struct Variable tmp;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case VEC4:       memcpy(dest, tmp.data.dvec4, sizeof(Quaternion)); return 1;
        case QUATERNION: memcpy(dest, tmp.data.dquaternion, sizeof(Quaternion)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_mat2(const struct Variable* var, Mat2 dest) {
    struct Variable tmp;
    float val;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   val = tmp.data.dint; goto scalar;
        case FLOAT: val = tmp.data.dfloat; goto scalar;
        scalar:     dest[0][0] = dest[0][1]
                  = dest[1][0] = dest[1][1] = val;
                    return 1;
        case MAT2:  memcpy(dest, tmp.data.dmat2, sizeof(Mat2)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_mat3(const struct Variable* var, Mat3 dest) {
    struct Variable tmp;
    float val;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   val = tmp.data.dint; goto scalar;
        case FLOAT: val = tmp.data.dfloat; goto scalar;
        scalar:     dest[0][0] = dest[0][1] = dest[0][2]
                  = dest[1][0] = dest[1][1] = dest[1][2]
                  = dest[2][0] = dest[2][1] = dest[2][2] = val;
                    return 1;
        case MAT3:  memcpy(dest, tmp.data.dmat3, sizeof(Mat3)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int variable_to_mat4(const struct Variable* var, Mat4 dest) {
    struct Variable tmp;
    float val;
    if (!variable_copy_dereference(var, &tmp)) return 0;
    switch (tmp.type) {
        case INT:   val = tmp.data.dint; goto scalar;
        case FLOAT: val = tmp.data.dfloat; goto scalar;
        scalar:     dest[0][0] = dest[0][1] = dest[0][2] = dest[0][3]
                  = dest[1][0] = dest[1][1] = dest[1][2] = dest[1][3]
                  = dest[2][0] = dest[2][1] = dest[2][2] = dest[2][3]
                  = dest[3][0] = dest[3][1] = dest[3][2] = dest[3][3] = val;
                    return 1;
        case MAT4:  memcpy(dest, tmp.data.dmat4, sizeof(Mat4)); return 1;
        default:;
    }
    variable_free(&tmp);
    return 0;
}

int uint_ptr_get(const void* src, struct Variable* dest) {
    dest->type = INT;
    dest->data.dint = *(const unsigned int*)src;
    return 1;
}

int float_ptr_get(const void* src, struct Variable* dest) {
    dest->type = FLOAT;
    dest->data.dfloat = *(const float*)src;
    return 1;
}

int float_ptr_set(const struct Variable* var, void* dest) {
    return variable_to_float(var, dest);
}

int vec3_ptr_get(const void* src, struct Variable* dest) {
    dest->type = VEC3;
    memcpy(dest->data.dvec3, src, sizeof(Vec3));
    return 1;
}

int vec3_ptr_set(const struct Variable* var, void* dest) {
    return variable_to_vec3(var, dest);
}

int vec3_ptr_set_normalize(const struct Variable* var, void* dest) {
    if (variable_to_vec3(var, dest)) {
        normalize3(dest);
        return 1;
    }
    return 0;
}

int quaternion_ptr_get(const void* src, struct Variable* dest) {
    dest->type = QUATERNION;
    memcpy(dest->data.dquaternion, src, sizeof(Quaternion));
    return 1;
}

int string_ptr_get(const void* src, struct Variable* dest) {
    const char* s = *(char**)src;
    if (!s) {
        s = "(none)";
    }
    return variable_make_string(s, strlen(s), dest);
}

int string_ptr_set(const struct Variable* src, void* dest) {
    char** d = dest;
    char* s;
    if (src->type != STRING || !(s = malloc(strlen(src->data.dstring) + 1))) return 0;
    strcpy(s, src->data.dstring);
    free(*d);
    *d = s;
    return 1;
}

void variable_make_int(long i, struct Variable* dest) {
    dest->type = INT;
    dest->data.dint = i;
}

void variable_make_float(float f, struct Variable* dest) {
    dest->type = FLOAT;
    dest->data.dfloat = f;
}

int variable_make_string(const char* s, size_t n, struct Variable* dest) {
    dest->type = STRING;
    if (n == ((size_t)-1) || !(dest->data.dstring = malloc(n + 1))) return 0;
    memcpy(dest->data.dstring, s, n);
    dest->data.dstring[n] = 0;
    return 1;
}

void variable_make_ptr(const struct PointerInfo* info, void* gdata, void* sdata, struct Variable* dest) {
    dest->type = POINTER;
    dest->data.dpointer.info = info;
    dest->data.dpointer.gdata = gdata;
    dest->data.dpointer.sdata = sdata;
}

void variable_make_struct(const struct StructInfo* info, void* data, struct Variable* dest) {
    dest->type = STRUCT;
    dest->data.dstruct.info = info;
    dest->data.dstruct.data = data;
}

void variable_make_array(const struct ArrayInfo* info, void* data, unsigned int* count, struct Variable* dest) {
    dest->type = ARRAY;
    dest->data.darray.info = info;
    dest->data.darray.data = data;
    dest->data.darray.count = count;
}

void variable_make_function(int (*f)(void*, const struct Variable*, unsigned int, struct Variable*), void* data, enum FunctionFlags flags, struct Variable* dest) {
    dest->type = FUNCTION;
    dest->data.dfunction.f = f;
    dest->data.dfunction.data = data;
    dest->data.dfunction.flags = flags;
}

int variable_copy(const struct Variable* src, struct Variable* dest) {
    dest->type = src->type;
    switch (src->type) {
        case INT: dest->data.dint = src->data.dint; return 1;
        case FLOAT: dest->data.dfloat = src->data.dfloat; return 1;
        case VEC2: memcpy(dest->data.dvec2, src->data.dvec2, sizeof(dest->data.dvec2)); return 1;
        case VEC3: memcpy(dest->data.dvec3, src->data.dvec3, sizeof(dest->data.dvec3)); return 1;
        case VEC4: memcpy(dest->data.dvec4, src->data.dvec4, sizeof(dest->data.dvec4)); return 1;
        case MAT2: memcpy(dest->data.dmat2, src->data.dmat2, sizeof(dest->data.dmat2)); return 1;
        case MAT3: memcpy(dest->data.dmat3, src->data.dmat3, sizeof(dest->data.dmat3)); return 1;
        case MAT4: memcpy(dest->data.dmat4, src->data.dmat4, sizeof(dest->data.dmat4)); return 1;
        case QUATERNION: memcpy(dest->data.dquaternion, src->data.dquaternion, sizeof(dest->data.dquaternion)); return 1;
        case STRING: return variable_make_string(src->data.dstring, strlen(src->data.dstring), dest);
        case POINTER:
            if (src->data.dpointer.info->copy) {
                return src->data.dpointer.info->copy(src, dest);
            }
            memcpy(dest, src, sizeof(*dest));
            return 1;
        case ARRAY:
            if (src->data.darray.info->copy) {
                return src->data.darray.info->copy(src, dest);
            }
            memcpy(dest, src, sizeof(*dest));
            return 1;
        case STRUCT:
            if (src->data.dstruct.info->copy) {
                return src->data.dstruct.info->copy(src, dest);
            }
            memcpy(dest, src, sizeof(*dest));
            return 1;
        case FUNCTION:
            variable_make_function(src->data.dfunction.f, src->data.dfunction.data, src->data.dfunction.flags, dest);
            if (src->data.dfunction.flags & FUNC_SIMPLE_COPY) {
                return 1;
            } else if (src->data.dfunction.flags & FUNC_DATAREF) {
                return shell_incref(src->data.dfunction.data);
            }
            return 0;
        case VOID: return 1;
    }
    return 0;
}

void variable_free(struct Variable* v) {
    switch (v->type) {
        case INT:
        case FLOAT:
        case VEC2:
        case VEC3:
        case VEC4:
        case MAT2:
        case MAT3:
        case MAT4:
        case QUATERNION:
            break;
        case STRING:
            free(v->data.dstring);
            break;
        case POINTER:
            if (v->data.dpointer.info->free) {
                v->data.dpointer.info->free(v);
            }
            break;
        case ARRAY:
            if (v->data.darray.info->free) {
                v->data.darray.info->free(v->data.darray.data);
            }
            break;
        case STRUCT:
            if (v->data.dstruct.info->free) {
                v->data.dstruct.info->free(v->data.dstruct.data);
            }
            break;
        case FUNCTION:
            if (v->data.dfunction.flags & FUNC_DATAREF) {
                shell_decref(v->data.dfunction.data);
            }
            break;
        case VOID:
            break;
    }
}
