#include "shell.h"

#ifndef TDSH_SKYBOX_H
#define TDSH_SKYBOX_H

int shell_make_skybox_vars(struct DemoShell* shell);

#endif
