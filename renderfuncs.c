#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <3dmr/render/viewer.h>
#include "builtins.h"
#include "shell.h"
#include "render.h"

static int vsync_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;

    if (!pthread_mutex_lock(&shell->swapMutex)) {
        while (shell->renderDone) {
            pthread_cond_wait(&shell->mainCond, &shell->swapMutex);
        }
        pthread_mutex_unlock(&shell->swapMutex);
    }
    ret->type = VOID;
    return 1;
}

static int screenshot_(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    char* path;
    int ok = 0;

    if (nArgs != 1 || !(path = variable_to_string(args))) {
        fprintf(stderr, "Error: screenshot(): expected path\n");
        return 0;
    }
    if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: failed to lock mutex\n");
    } else {
        viewer_make_current(shell->viewer);
        if (!(ok = viewer_screenshot(shell->viewer, path))) {
            fprintf(stderr, "Error: failed to take screenshot\n");
        }
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    ret->type = VOID;
    free(path);
    return ok;
}

int shell_make_render_functions(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "vsync" , vsync_, shell)
        && builtin_function_with_data(shell, "screenshot" , screenshot_, shell);
}
