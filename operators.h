#include "variable.h"

#ifndef TDSH_OPERATORS_H
#define TDSH_OPERATORS_H

int variable_assign(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);

int variable_add(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_sub(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_mul(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_div(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_mod(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);

int variable_addeq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_subeq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_muleq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_diveq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_modeq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);

int variable_preincr(struct Variable* v, struct Variable* dest, char** err);
int variable_predecr(struct Variable* v, struct Variable* dest, char** err);
int variable_postincr(struct Variable* v, struct Variable* dest, char** err);
int variable_postdecr(struct Variable* v, struct Variable* dest, char** err);

int variable_lt(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_gt(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_le(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_ge(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_eq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_neq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_and(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
int variable_or(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);

int variable_unaryplus(struct Variable* v, struct Variable* dest, char** err);
int variable_neg(struct Variable* v, struct Variable* dest, char** err);
int variable_not(struct Variable* v, struct Variable* dest, char** err);

int variable_struct_member(const struct Variable* s, const char* member, struct Variable* dest, char** err);
int variable_array_member(const struct Variable* a, const struct Variable* member, struct Variable* dest, char** err);

#endif
