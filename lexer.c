#include <stdio.h>
#include "lexer.h"

int next_token(const char* s, union Token* token, char** end) {
    while (*s == ' ' || *s == '\t') {
        s++;
    }
    switch (*s) {
        case 0:
        case '#':
        case '\n':
            return EOF;
        case '(':
        case ')':
        case '[':
        case ']':
        case '{':
        case '}':
            if (end) *end = (char*)s + 1;
            if (token) token->s = (char*)s;
            return *s;
        case '+':
        case '-':
            if (end) *end = (char*)s + 1 + (s[1] == '=' || s[1] == s[0]);
            if (token) token->s = (char*)s;
            return (s[1] == '=' || s[1] == s[0]) ? 'P' : 'O';
        case '*':
        case '/':
        case '%':
        case '<':
        case '>':
        case '=':
        case '!':
            if (end) *end = (char*)s + 1 + (s[1] == '=');
            if (token) token->s = (char*)s;
            return (s[1] == '=') ? 'P' : 'O';
        case '&':
        case '|':
            if (s[1] != s[0]) break;
            if (end) *end = (char*)s + 2;
            if (token) token->s = (char*)s;
            return 'P';
        case ':':
            if (s[1] == '=') {
                if (end) *end = (char*)s + 2;
                if (token) token->s = (char*)s;
                return 'P';
            }
            return 0;
        case ',':
        case ';':
        case '.':
            if (end) *end = (char*)s + 1;
            if (token) token->s = (char*)s;
            return 'O';
        case '"':
            if (token) token->s = (char*)s + 1;
            for (++s; *s && (*s != '"' || s[-1] == '\\'); s++);
            if (end) *end = (char*)s + (*s == '"');
            return (*s == '"') ? 'S' : 0;
    }
    if (*s >= '0' && *s <= '9') {
        if (token) token->i = 0;
        do {
            if (token) token->i = token->i * 10 + (*s - '0');
            s++;
        } while (*s >= '0' && *s <= '9');
        if (*s == '.') {
            float factor = 0.1f;
            if (token) token->f = token->i;
            for (s++; *s >= '0' && *s <= '9'; s++) {
                if (token) token->f += factor * ((float)(*s - '0'));
                factor /= 10.0f;
            }
            if (end) *end = (char*)s;
            return 'F';
        }
        if (end) *end = (char*)s;
        return 'I';
    } else if (*s == '_' || (*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z')) {
        if (token) token->s = (char*)s;
        for (s++; *s == '_' || (*s >= 'a' && *s <= 'z') || (*s >= 'A' && *s <= 'Z') || (*s >= '0' && *s <= '9'); s++);
        if (end) *end = (char*)s;
        return 'N';
    }
    return 0;
}
