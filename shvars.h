#include "shell.h"

#ifndef TDSH_SHVARS_H
#define TDSH_SHVARS_H

int shell_make_shvars(struct DemoShell* shell);

#endif
