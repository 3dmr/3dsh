#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include "variable.h"

static void set_error(char** err, size_t size, const char* format, ...) {
    va_list ap;
    if (err && (*err = malloc(size))) {
        va_start(ap, format);
        vsprintf(*err, format, ap);
        va_end(ap);
    }
}

int variable_assign(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    struct Variable tb;
    int pb;

    if (a->type != POINTER || !a->data.dpointer.info->set) {
        set_error(err, 64, "not a lvalue");
        return 0;
    }
    pb = variable_move_dereference(b, &tb);
    if (tb.type == POINTER) {
        set_error(err, 64, "failed to deref pointer");
        if (pb) variable_free(&tb);
        return 0;
    }
    if (!variable_pointer_set(&tb, a)) {
        set_error(err, 64, "assignement failed, check types");
        if (pb) variable_free(&tb);
        return 0;
    }
    if (pb) {
        *dest = tb;
    } else if (!variable_copy(&tb, dest)) {
        set_error(err, 64, "failed to copy variable");
        return 0;
    }
    return 1;
}

static int is_scalar(enum VarType type) {
    return type == INT || type == FLOAT;
}

static float get_scalar(const struct Variable* var) {
    return (var->type == INT) ? ((float)var->data.dint) : var->data.dfloat;
}

struct BinaryOperator {
    long (*int_int)(long, long);
    float (*float_float)(float, float);
    void (*vec2)(Vec2, const Vec2, const Vec2);
    void (*vec3)(Vec3, const Vec3, const Vec3);
    void (*vec4)(Vec4, const Vec4, const Vec4);
    void (*vec2_float)(Vec2, const Vec2, float);
    void (*vec3_float)(Vec3, const Vec3, float);
    void (*vec4_float)(Vec4, const Vec4, float);
    void (*float_vec2)(Vec2, float, const Vec2);
    void (*float_vec3)(Vec3, float, const Vec3);
    void (*float_vec4)(Vec4, float, const Vec4);
    void (*mat2)(Mat2, const Mat2, const Mat2);
    void (*mat3)(Mat3, const Mat3, const Mat3);
    void (*mat4)(Mat4, const Mat4, const Mat4);
    void (*mat2_float)(Mat2, const Mat2, float);
    void (*mat3_float)(Mat3, const Mat3, float);
    void (*mat4_float)(Mat4, const Mat4, float);
    void (*float_mat2)(Mat2, float, const Mat2);
    void (*float_mat3)(Mat3, float, const Mat3);
    void (*float_mat4)(Mat4, float, const Mat4);
    void (*mat2_vec2)(Vec2, const Mat2, const Vec2);
    void (*mat3_vec3)(Vec3, const Mat3, const Vec3);
    void (*mat4_vec4)(Vec4, const Mat4, const Vec4);
    void (*quat_quat)(Quaternion, const Quaternion, const Quaternion);
    void (*quat_vec3)(Vec3, const Quaternion, const Vec3);
    void (*quat_vec4)(Vec4, const Quaternion, const Vec4);
    char symbol;
};

static int variable_binary_op(const struct BinaryOperator* op, struct Variable* va, struct Variable* vb, struct Variable* dest, char** err) {
    struct Variable a, b;
    int ret = 1, pa, pb;

    pa = variable_move_dereference(va, &a);
    pb = variable_move_dereference(vb, &b);

    if (a.type == INT && b.type == INT) {
        variable_make_int(op->int_int(a.data.dint, b.data.dint), dest);
    } else if (is_scalar(a.type) && is_scalar(b.type)) {
        variable_make_float(op->float_float(get_scalar(&a), get_scalar(&b)), dest);
    } else if (a.type == VEC2 && b.type == VEC2 && op->vec2) {
        dest->type = VEC2; op->vec2(dest->data.dvec2, a.data.dvec2, b.data.dvec2);
    } else if (a.type == VEC3 && b.type == VEC3 && op->vec3) {
        dest->type = VEC3; op->vec3(dest->data.dvec3, a.data.dvec3, b.data.dvec3);
    } else if (a.type == VEC4 && b.type == VEC4 && op->vec4) {
        dest->type = VEC4; op->vec4(dest->data.dvec4, a.data.dvec4, b.data.dvec4);
    } else if (a.type == VEC2 && is_scalar(b.type) && op->vec2_float) {
        dest->type = VEC2; op->vec2_float(dest->data.dvec2, a.data.dvec2, get_scalar(&b));
    } else if (a.type == VEC3 && is_scalar(b.type) && op->vec3_float) {
        dest->type = VEC3; op->vec3_float(dest->data.dvec3, a.data.dvec3, get_scalar(&b));
    } else if (a.type == VEC4 && is_scalar(b.type) && op->vec4_float) {
        dest->type = VEC4; op->vec4_float(dest->data.dvec4, a.data.dvec4, get_scalar(&b));
    } else if (is_scalar(a.type) && b.type == VEC2 && op->float_vec2) {
        dest->type = VEC2; op->float_vec2(dest->data.dvec2, get_scalar(&a), b.data.dvec2);
    } else if (is_scalar(a.type) && b.type == VEC3 && op->float_vec3) {
        dest->type = VEC3; op->float_vec3(dest->data.dvec3, get_scalar(&a), b.data.dvec3);
    } else if (is_scalar(a.type) && b.type == VEC4 && op->float_vec4) {
        dest->type = VEC4; op->float_vec4(dest->data.dvec4, get_scalar(&a), b.data.dvec4);
    } else if (a.type == MAT2 && b.type == MAT2 && op->mat2) {
        dest->type = MAT2; op->mat2(dest->data.dmat2, MAT_CONST_CAST(a.data.dmat2), MAT_CONST_CAST(b.data.dmat2));
    } else if (a.type == MAT3 && b.type == MAT3 && op->mat3) {
        dest->type = MAT3; op->mat3(dest->data.dmat3, MAT_CONST_CAST(a.data.dmat3), MAT_CONST_CAST(b.data.dmat3));
    } else if (a.type == MAT4 && b.type == MAT4 && op->mat4) {
        dest->type = MAT4; op->mat4(dest->data.dmat4, MAT_CONST_CAST(a.data.dmat4), MAT_CONST_CAST(b.data.dmat4));
    } else if (a.type == MAT2 && is_scalar(b.type) && op->mat2_float) {
        dest->type = MAT2; op->mat2_float(dest->data.dmat2, MAT_CONST_CAST(a.data.dmat2), get_scalar(&b));
    } else if (a.type == MAT3 && is_scalar(b.type) && op->mat3_float) {
        dest->type = MAT3; op->mat3_float(dest->data.dmat3, MAT_CONST_CAST(a.data.dmat3), get_scalar(&b));
    } else if (a.type == MAT4 && is_scalar(b.type) && op->mat4_float) {
        dest->type = MAT4; op->mat4_float(dest->data.dmat4, MAT_CONST_CAST(a.data.dmat4), get_scalar(&b));
    } else if (is_scalar(a.type) && b.type == MAT2 && op->float_mat2) {
        dest->type = MAT2; op->float_mat2(dest->data.dmat2, get_scalar(&a), MAT_CONST_CAST(b.data.dmat2));
    } else if (is_scalar(a.type) && b.type == MAT3 && op->float_mat3) {
        dest->type = MAT3; op->float_mat3(dest->data.dmat3, get_scalar(&a), MAT_CONST_CAST(b.data.dmat3));
    } else if (is_scalar(a.type) && b.type == MAT4 && op->float_mat4) {
        dest->type = MAT4; op->float_mat4(dest->data.dmat4, get_scalar(&a), MAT_CONST_CAST(b.data.dmat4));
    } else if (a.type == MAT2 && b.type == VEC2 && op->mat2_vec2) {
        dest->type = VEC2; op->mat2_vec2(dest->data.dvec2, MAT_CONST_CAST(a.data.dmat2), b.data.dvec2);
    } else if (a.type == MAT3 && b.type == VEC3 && op->mat3_vec3) {
        dest->type = VEC3; op->mat3_vec3(dest->data.dvec3, MAT_CONST_CAST(a.data.dmat3), b.data.dvec3);
    } else if (a.type == MAT4 && b.type == VEC4 && op->mat4_vec4) {
        dest->type = VEC4; op->mat4_vec4(dest->data.dvec4, MAT_CONST_CAST(a.data.dmat4), b.data.dvec4);
    } else if (a.type == QUATERNION && b.type == QUATERNION && op->quat_quat) {
        dest->type = QUATERNION; op->quat_quat(dest->data.dquaternion, a.data.dquaternion, b.data.dquaternion);
    } else if (a.type == QUATERNION && b.type == VEC3 && op->quat_vec3) {
        dest->type = VEC3; op->quat_vec3(dest->data.dvec3, a.data.dquaternion, b.data.dvec3);
    } else if (a.type == QUATERNION && b.type == VEC4 && op->quat_vec4) {
        dest->type = VEC4; op->quat_vec4(dest->data.dvec4, a.data.dquaternion, b.data.dvec4);
    } else if (a.type == POINTER || b.type == POINTER) {
        set_error(err, 64, "failed to deref pointer");
        ret = 0;
    } else {
        const char* ta = variable_type_string(a.type);
        const char* tb = variable_type_string(b.type);
        if (ta && tb) {
            set_error(err, 64 + strlen(tb) + strlen(tb), "cannot do %s %c %s", ta, op->symbol, tb);
        } else {
            set_error(err, 64, "bad types");
        }
        ret = 0;
    }
    if (pa) variable_free(&a);
    if (pb) variable_free(&b);
    return ret;
}

static int variable_binary_opassign(const struct BinaryOperator* op, struct Variable* va, struct Variable* vb, struct Variable* dest, char** err) {
    if (!variable_binary_op(op, va, vb, dest, err)) return 0;
    variable_free(dest);
    return variable_assign(va, dest, dest, err);
}

static long add_int(long a, long b) {return a + b;}
static float add_float(float a, float b) {return a + b;}
static void add2vs(Vec2 d, const Vec2 a, float b) {d[0] = a[0] + b; d[1] = a[1] + b;}
static void add3vs(Vec3 d, const Vec3 a, float b) {d[0] = a[0] + b; d[1] = a[1] + b; d[2] = a[2] + b;}
static void add4vs(Vec4 d, const Vec4 a, float b) {d[0] = a[0] + b; d[1] = a[1] + b; d[2] = a[2] + b; d[3] = a[3] + b;}
static void add2sv(Vec2 d, float a, const Vec2 b) {add2vs(d, b, a);}
static void add3sv(Vec3 d, float a, const Vec3 b) {add3vs(d, b, a);}
static void add4sv(Vec4 d, float a, const Vec4 b) {add4vs(d, b, a);}
static void add2m(Mat2 d, const Mat2 a, const Mat2 b) {add2v(d[0], a[0], b[0]); add2v(d[1], a[1], b[1]);}
static void add3m(Mat3 d, const Mat3 a, const Mat3 b) {add3v(d[0], a[0], b[0]); add3v(d[1], a[1], b[1]); add3v(d[2], a[2], b[2]);}
static void add4m(Mat4 d, const Mat4 a, const Mat4 b) {add4v(d[0], a[0], b[0]); add4v(d[1], a[1], b[1]); add4v(d[2], a[2], b[2]); add4v(d[3], a[3], b[3]);}
static void add2ms(Mat2 d, const Mat2 a, float b) {add2vs(d[0], a[0], b); add2vs(d[1], a[1], b);}
static void add3ms(Mat3 d, const Mat3 a, float b) {add3vs(d[0], a[0], b); add3vs(d[1], a[1], b); add3vs(d[2], a[2], b);}
static void add4ms(Mat4 d, const Mat4 a, float b) {add4vs(d[0], a[0], b); add4vs(d[1], a[1], b); add4vs(d[2], a[2], b); add4vs(d[3], a[3], b);}
static void add2sm(Mat2 d, float a, const Mat2 b) {add2ms(d, b, a);}
static void add3sm(Mat3 d, float a, const Mat3 b) {add3ms(d, b, a);}
static void add4sm(Mat4 d, float a, const Mat4 b) {add4ms(d, b, a);}
static struct BinaryOperator add = {
    add_int, add_float,
    add2v, add3v, add4v,
    add2vs, add3vs, add4vs,
    add2sv, add3sv, add4sv,
    add2m, add3m, add4m,
    add2ms, add3ms, add4ms,
    add2sm, add3sm, add4sm,
    0, 0, 0,
    0, 0, 0,
    '+'
};

int variable_add(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_op(&add, a, b, dest, err);
}

int variable_addeq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_opassign(&add, a, b, dest, err);
}

static long sub_int(long a, long b) {return a - b;}
static float sub_float(float a, float b) {return a - b;}
static void sub2vs(Vec2 d, const Vec2 a, float b) {d[0] = a[0] - b; d[1] = a[1] - b;}
static void sub3vs(Vec3 d, const Vec3 a, float b) {d[0] = a[0] - b; d[1] = a[1] - b; d[2] = a[2] - b;}
static void sub4vs(Vec4 d, const Vec4 a, float b) {d[0] = a[0] - b; d[1] = a[1] - b; d[2] = a[2] - b; d[3] = a[3] - b;}
static void sub2sv(Vec2 d, float a, const Vec2 b) {d[0] = a - b[0]; d[1] = a - b[1];}
static void sub3sv(Vec3 d, float a, const Vec3 b) {d[0] = a - b[0]; d[1] = a - b[1]; d[2] = a - b[2];}
static void sub4sv(Vec4 d, float a, const Vec4 b) {d[0] = a - b[0]; d[1] = a - b[1]; d[2] = a - b[2]; d[3] = a - b[3];}
static void sub2m(Mat2 d, const Mat2 a, const Mat2 b) {sub2v(d[0], a[0], b[0]); sub2v(d[1], a[1], b[1]);}
static void sub3m(Mat3 d, const Mat3 a, const Mat3 b) {sub3v(d[0], a[0], b[0]); sub3v(d[1], a[1], b[1]); sub3v(d[2], a[2], b[2]);}
static void sub4m(Mat4 d, const Mat4 a, const Mat4 b) {sub4v(d[0], a[0], b[0]); sub4v(d[1], a[1], b[1]); sub4v(d[2], a[2], b[2]); sub4v(d[3], a[3], b[3]);}
static void sub2ms(Mat2 d, const Mat2 a, float b) {sub2vs(d[0], a[0], b); sub2vs(d[1], a[1], b);}
static void sub3ms(Mat3 d, const Mat3 a, float b) {sub3vs(d[0], a[0], b); sub3vs(d[1], a[1], b); sub3vs(d[2], a[2], b);}
static void sub4ms(Mat4 d, const Mat4 a, float b) {sub4vs(d[0], a[0], b); sub4vs(d[1], a[1], b); sub4vs(d[2], a[2], b); sub4vs(d[3], a[3], b);}
static void sub2sm(Mat2 d, float a, const Mat2 b) {sub2sv(d[0], a, b[0]); sub2sv(d[1], a, b[1]);}
static void sub3sm(Mat3 d, float a, const Mat3 b) {sub3sv(d[0], a, b[0]); sub3sv(d[1], a, b[1]); sub3sv(d[2], a, b[2]);}
static void sub4sm(Mat4 d, float a, const Mat4 b) {sub4sv(d[0], a, b[0]); sub4sv(d[1], a, b[1]); sub4sv(d[2], a, b[2]); sub4sv(d[3], a, b[3]);}
static struct BinaryOperator sub = {
    sub_int, sub_float,
    sub2v, sub3v, sub4v,
    sub2vs, sub3vs, sub4vs,
    sub2sv, sub3sv, sub4sv,
    sub2m, sub3m, sub4m,
    sub2ms, sub3ms, sub4ms,
    sub2sm, sub3sm, sub4sm,
    0, 0, 0,
    0, 0, 0,
    '-'
};

int variable_sub(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_op(&sub, a, b, dest, err);
}

int variable_subeq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_opassign(&sub, a, b, dest, err);
}

static long mul_int(long a, long b) {return a * b;}
static float mul_float(float a, float b) {return a * b;}
static void mul2vs(Vec2 d, const Vec2 a, float b) {mul2sv(d, b, a);}
static void mul3vs(Vec3 d, const Vec3 a, float b) {mul3sv(d, b, a);}
static void mul4vs(Vec4 d, const Vec4 a, float b) {mul4sv(d, b, a);}
static void mul2ms(Mat2 d, const Mat2 a, float b) {mul2sm(d, b, a);}
static void mul3ms(Mat3 d, const Mat3 a, float b) {mul3sm(d, b, a);}
static void mul4ms(Mat4 d, const Mat4 a, float b) {mul4sm(d, b, a);}
static void mulqv4(Vec4 d, const Quaternion a, const Vec4 b) {
    Vec3 tmp;
    mul3sv(tmp, 1.0f / b[3], b);
    quaternion_compose(d, a, tmp);
    d[3] = 1;
}
static struct BinaryOperator mul = {
    mul_int, mul_float,
    0, 0, 0,
    mul2vs, mul3vs, mul4vs,
    mul2sv, mul3sv, mul4sv,
    mul2mm, mul3mm, mul4mm,
    mul2ms, mul3ms, mul4ms,
    mul2sm, mul3sm, mul4sm,
    mul2mv, mul3mv, mul4mv,
    quaternion_mul, quaternion_compose, mulqv4,
    '*'
};

int variable_mul(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_op(&mul, a, b, dest, err);
}

int variable_muleq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_opassign(&mul, a, b, dest, err);
}

static long div_int(long a, long b) {return a / b;}
static float div_float(float a, float b) {return a / b;}
static void div2vs(Vec2 d, const Vec2 a, float b) {mul2sv(d, 1.0f / b, a);}
static void div3vs(Vec3 d, const Vec3 a, float b) {mul3sv(d, 1.0f / b, a);}
static void div4vs(Vec4 d, const Vec4 a, float b) {mul4sv(d, 1.0f / b, a);}
static void div2ms(Mat2 d, const Mat2 a, float b) {mul2sm(d, 1.0f / b, a);}
static void div3ms(Mat3 d, const Mat3 a, float b) {mul3sm(d, 1.0f / b, a);}
static void div4ms(Mat4 d, const Mat4 a, float b) {mul4sm(d, 1.0f / b, a);}
void quaternion_div(Quaternion dest, const Quaternion a, const Quaternion b) {
    Quaternion invB;
    memcpy(invB, b, sizeof(invB));
    quaternion_inv(invB);
    quaternion_mul(dest, invB, a);
}
static struct BinaryOperator div_ = {
    div_int, div_float,
    0, 0, 0,
    div2vs, div3vs, div4vs,
    0, 0, 0,
    0, 0, 0,
    div2ms, div3ms, div4ms,
    0, 0, 0,
    0, 0, 0,
    quaternion_div, 0, 0,
    '/'
};

int variable_div(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_op(&div_, a, b, dest, err);
}

int variable_diveq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_opassign(&div_, a, b, dest, err);
}

static long mod_int(long a, long b) {return a % b;}
static float mod_float(float a, float b) {return fmod(a, b);}
static struct BinaryOperator mod_ = {
    mod_int, mod_float,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    0, 0, 0,
    '%'
};

int variable_mod(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_op(&mod_, a, b, dest, err);
}

int variable_modeq(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) {
    return variable_binary_opassign(&mod_, a, b, dest, err);
}

int variable_preincr(struct Variable* v, struct Variable* dest, char** err) {
    struct Variable one;
    variable_make_int(1, &one);
    return variable_addeq(v, &one, dest, err);
}

int variable_predecr(struct Variable* v, struct Variable* dest, char** err) {
    struct Variable one;
    variable_make_int(1, &one);
    return variable_subeq(v, &one, dest, err);
}

int variable_postincr(struct Variable* v, struct Variable* dest, char** err) {
    struct Variable one, res;
    int ok;
    variable_make_int(1, &one);
    if (!variable_copy_dereference(v, dest)) {
        set_error(err, 64, "failed to copy variable");
        return 0;
    }
    if (!variable_add(v, &one, &res, err)) {
        variable_free(dest);
        return 0;
    }
    ok = variable_assign(v, &res, &one, err);
    variable_free(&res);
    if (!ok) {
        variable_free(&one);
        variable_free(dest);
        return 0;
    }
    return 1;
}

int variable_postdecr(struct Variable* v, struct Variable* dest, char** err) {
    struct Variable one, res;
    int ok;
    variable_make_int(1, &one);
    if (!variable_copy_dereference(v, dest)) {
        set_error(err, 64, "failed to copy variable");
        return 0;
    }
    if (!variable_sub(v, &one, &res, err)) {
        variable_free(dest);
        return 0;
    }
    ok = variable_assign(v, &res, &one, err);
    variable_free(&res);
    if (!ok) {
        variable_free(&one);
        variable_free(dest);
        return 0;
    }
    return 1;
}

#define binop_int(fname, op) \
int variable_##fname(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) { \
    struct Variable ta, tb; \
    int pa = variable_move_dereference(a, &ta); \
    int pb = variable_move_dereference(b, &tb); \
    if (ta.type == INT && tb.type == INT) { \
        variable_make_int(ta.data.dint op tb.data.dint, dest); \
        return 1; \
    } \
    if (pa) variable_free(&ta); \
    if (pb) variable_free(&tb); \
    set_error(err, 64, "expected int"); \
    return 0; \
}

#define binop_scalar(fname, op) \
int variable_##fname(struct Variable* a, struct Variable* b, struct Variable* dest, char** err) { \
    struct Variable ta, tb; \
    int pa = variable_move_dereference(a, &ta); \
    int pb = variable_move_dereference(b, &tb); \
    if (ta.type == INT && tb.type == INT) { \
        variable_make_int(ta.data.dint op tb.data.dint, dest); \
        return 1; \
    } else if (is_scalar(ta.type) && is_scalar(tb.type)) { \
        variable_make_int(get_scalar(&ta) op get_scalar(&tb), dest); \
        return 1; \
    } \
    if (pa) variable_free(&ta); \
    if (pb) variable_free(&tb); \
    set_error(err, 64, "expected int"); \
    return 0; \
}

binop_scalar(lt, <)
binop_scalar(gt, >)
binop_scalar(le, <=)
binop_scalar(ge, >=)
binop_scalar(eq, ==)
binop_scalar(neq, ==)
binop_int(and, &&)
binop_int(or, ||)

int variable_unaryplus(struct Variable* v, struct Variable* dest, char** err) {
    int p = variable_move_dereference(v, dest);

    switch (dest->type) {
        case INT:
        case FLOAT:
        case VEC2:
        case VEC3:
        case VEC4:
        case MAT2:
        case MAT3:
        case MAT4:
            return 1;
        case POINTER:
            set_error(err, 64, "failed to deref pointer");
            break;
        default:
            set_error(err, 64, "invalid type for unary+");
    }
    if (p) variable_free(dest);
    return 0;
}

int variable_neg(struct Variable* v, struct Variable* dest, char** err) {
    int p = variable_move_dereference(v, dest);

    switch (dest->type) {
        case INT:   dest->data.dint = -(dest->data.dint); return 1;
        case FLOAT: dest->data.dfloat = -(dest->data.dfloat); return 1;
        case VEC2:  neg2v(dest->data.dvec2); return 1;
        case VEC3:  neg3v(dest->data.dvec3); return 1;
        case VEC4:  neg4v(dest->data.dvec4); return 1;
        case MAT2:  neg2m(dest->data.dmat2); return 1;
        case MAT3:  neg3m(dest->data.dmat3); return 1;
        case MAT4:  neg4m(dest->data.dmat4); return 1;
        case POINTER: set_error(err, 64, "failed to deref pointer"); break;
        default:      set_error(err, 64, "invalid type for unary-"); break;
    }

    if (p) variable_free(dest);
    return 0;
}

int variable_not(struct Variable* v, struct Variable* dest, char** err) {
    int p = variable_move_dereference(v, dest);

    switch (dest->type) {
        case INT:   dest->data.dint = !(dest->data.dint); return 1;
        case POINTER: set_error(err, 64, "failed to deref pointer"); break;
        default:      set_error(err, 64, "invalid type for unary-"); break;
    }

    if (p) variable_free(dest);
    return 0;
}

int variable_struct_member(const struct Variable* s, const char* member, struct Variable* dest, char** err) {
    struct Variable ts;
    const struct FieldInfo* field;
    int ps = variable_move_dereference(s, &ts);

    if (ts.type != STRUCT) {
        set_error(err, 64, "not a struct");
        if (ps) variable_free(&ts);
        return 0;
    }
    field = ts.data.dstruct.info->fields;
    while (field->name) {
        if (!strcmp(field->name, member)) {
            if (variable_struct_get(&ts, field, dest)) {
                if (ps) variable_free(&ts);
                return 1;
            }
            set_error(err, 64, "failed to retrieve struct member");
            if (ps) variable_free(&ts);
            return 0;
        }
        field++;
    }
    set_error(err, 64 + strlen(member), "no member named '%s'", member);
    if (ps) variable_free(&ts);
    return 0;
}

int variable_array_member(const struct Variable* a, const struct Variable* member, struct Variable* dest, char** err) {
    struct Variable ta, tm;
    const float* ptr;
    unsigned long n;
    long i;
    int pa, pm, ret = 0;

    pm = variable_move_dereference(member, &tm);
    if (tm.type != INT) {
        if (tm.type == POINTER) {
            set_error(err, 64, "failed to deref pointer");
        } else {
            set_error(err, 64, "index should be an integer");
        }
        if (pm) variable_free(&tm);
        return 0;
    }
    i = tm.data.dint;

    pa = variable_move_dereference(a, &ta);
    switch (ta.type) {
        case ARRAY:
            if (i < 0 || ((unsigned long)i) >= ((unsigned long)(*(ta.data.darray.count)))) {
                set_error(err, 128, "index %ld is out of bounds", i);
            } else if (!variable_array_get(&ta, i, dest)) {
                set_error(err, 128, "failed to get index %ld", i);
            } else {
                ret = 1;
            }
            break;

        case VEC2: n = 2; ptr = ta.data.dvec2; goto array_vec;
        case VEC3: n = 3; ptr = ta.data.dvec3; goto array_vec;
        case VEC4: n = 4; ptr = ta.data.dvec4; goto array_vec;
array_vec:
            if (i < 0 || ((unsigned long)i) >= n) {
                set_error(err, 128, "index %ld is out of bounds", i);
            } else {
                variable_make_float(ptr[i], dest);
                ret = 1;
            }
            break;

        case MAT2:
            if (i < 0 || ((unsigned long)i) >= 2) {
                set_error(err, 128, "index %ld is out of bounds", i);
            } else {
                dest->type = VEC2;
                memcpy(dest->data.dvec2, ta.data.dmat2[i], sizeof(Vec2));
                ret = 1;
            }
            break;

        case MAT3:
            if (i < 0 || ((unsigned long)i) >= 3) {
                set_error(err, 128, "index %ld is out of bounds", i);
            } else {
                dest->type = VEC3;
                memcpy(dest->data.dvec3, ta.data.dmat3[i], sizeof(Vec3));
                ret = 1;
            }
            break;

        case MAT4:
            if (i < 0 || ((unsigned long)i) >= 4) {
                set_error(err, 128, "index %ld is out of bounds", i);
            } else {
                dest->type = VEC4;
                memcpy(dest->data.dvec4, ta.data.dmat4[i], sizeof(Vec4));
                ret = 1;
            }
            break;

        case POINTER:
            set_error(err, 64, "failed to deref pointer");
            break;

        default:
            set_error(err, 64, "not subscriptable");
    }
    if (pm) variable_free(&tm);
    if (pa) variable_free(&ta);
    return ret;
}
