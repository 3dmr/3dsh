#include "shell.h"

#ifndef TDSH_BUILTINS_H
#define TDSH_BUILTINS_H

int builtin_int(struct DemoShell* shell, const char* name, long v);
int builtin_float(struct DemoShell* shell, const char* name, float v);
int builtin_vec3(struct DemoShell* shell, const char* name, float x, float y, float z);
int builtin_ptr(struct DemoShell* shell, const char* name, const struct PointerInfo* info, void* gdata, void* sdata);
int builtin_function(struct DemoShell* shell, const char* name, int (*f)(void*, const struct Variable*, unsigned int, struct Variable*));
int builtin_function_with_data(struct DemoShell* shell, const char* name, int (*f)(void*, const struct Variable*, unsigned int, struct Variable*), void* data);
int builtin_struct(struct DemoShell* shell, const char* name, const struct StructInfo* sinfo, void* data);

int shell_make_builtins(struct DemoShell* shell);

#endif
