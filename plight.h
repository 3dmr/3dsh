#include "shell.h"

#ifndef TDSH_PLIGHT_H
#define TDSH_PLIGHT_H

int shell_make_plights(struct DemoShell* shell);

int variable_is_plight(const struct Variable* var);
struct PointLight* variable_to_plight(const struct Variable* var);
int variable_set_plight_index(const struct Variable* var, unsigned int i);
int variable_make_plight(struct DemoShell* shell, const struct PointLight* pl, struct Variable* dest);

#endif
