#include "variable.h"

#ifndef TDSH_PARSER_H
#define TDSH_PARSER_H

struct AstNode {
    enum AstNodeType {BLOC, CONTROL_FLOW, IDENTIFIER, FUNCTION_CALL, BINARY_OP, UNARY_OP, ARRAY_ACCESS, STRUCT_MEMBER, TUPLE, CONSTANT, FUNCTION_DECL} type;
    union AstNodeData {
        struct BlocNode {
            struct AstNode** list;
            unsigned int count;
            int open;
        } bloc;
        struct ControlFlowNode {
            enum ControlFlowType {IF = 1, FOR, WHILE} type;
            struct AstNode *condition, *body, *elseBody;
        } controlFlow;
        struct IdentifierNode {
            char* name;
            int isDecl;
        } identifier;
        struct FunctionCallNode {
            char* function;
            struct AstNode* args;
        } functionCall;
        struct BinaryOpNode {
            int (*op)(struct Variable* a, struct Variable* b, struct Variable* dest, char** err);
            struct AstNode *a, *b;
        } binaryOp;
        struct UnaryOpNode {
            int (*op)(struct Variable* v, struct Variable* dest, char** err);
            struct AstNode* v;
        } unaryOp;
        struct ArrayAccessNode {
            struct AstNode *array, *index;
        } arrayAccess;
        struct StructMemberNode {
            struct AstNode *s, *member;
        } structMember;
        struct TupleNode {
            struct AstNode** list;
            unsigned int count;
            int open;
        } tuple;
        struct ConstantNode {
            struct Variable value;
        } constant;
        struct FunctionDeclNode {
            struct AstNode *arglist, *body;
        } functionDecl;
    } data;
};

struct Parser {
    struct AstNode** expressions;
    char** operators;
    unsigned int numExprs, numOps;
    enum ParserStatus {PARSE_OPERAND, PARSE_OPERATOR, PARSE_DONE, PARSE_ERROR} status;
    const char* error;
};

void parser_init(struct Parser* parser);
void parse(struct Parser* parser, const char* command);
int parse_need_more(const struct Parser* parser);
int parse_empty(const struct Parser* parser);
struct AstNode* parse_end(struct Parser* parser);
void parser_free(struct Parser* parser);

struct AstNode* ast_deep_copy(const struct AstNode* root);
void ast_free(struct AstNode* root);

#endif
