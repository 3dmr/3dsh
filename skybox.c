#include <stdio.h>
#include <stdlib.h>
#include <3dmr/skybox.h>
#include "builtins.h"
#include "shell.h"
#include "variable.h"
#include "render.h"

struct SkyboxMaterial {
    struct Material material;
    GLuint texture;
};

static struct Skybox* do_skybox(struct DemoShell* shell, GLuint texture) {
    struct Skybox* out = NULL;
    if (texture) {
        GLint size;
        out = skybox_new(texture);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
        glGetTexLevelParameteriv(GL_TEXTURE_CUBE_MAP, 0, GL_TEXTURE_WIDTH, &size);
        if (size < 256) {
            size = 256;
        } else if (size > 1024) {
            size = 1024;
        }
        if (shell->ibl.enabled) {
            glDeleteTextures(1, &shell->ibl.irradianceMap);
            glDeleteTextures(1, &shell->ibl.specularMap);
            glDeleteTextures(1, &shell->ibl.specularBrdf);
        }
        if (!compute_ibl(texture, 32, size, 5, 256, &shell->ibl)) {
            shell->ibl.enabled = 0;
        }
    }
    return out;
}

static int skybox(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct Skybox* new = NULL;
    char* paths[6];
    GLuint texture;
    unsigned int i;

    if (!nArgs) {
        if (pthread_mutex_lock(&shell->swapMutex)) {
            fprintf(stderr, "Error: failed to lock mutex\n");
            return 0;
        }
        viewer_make_current(shell->viewer);
        skybox_free(shell->skybox);
        shell->skybox = NULL;
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
        ret->type = VOID;
        return 1;
    }

    if (nArgs != 1 && nArgs != 6) {
        fprintf(stderr, "Error: expected 1 path (HDR equirect) or 6 paths (cubemap, non HDR)\n");
        return 0;
    }
    for (i = 0; i < nArgs; i++) {
        if (!(paths[i] = variable_to_string(args + i))) {
            while (i) free(paths[--i]);
            fprintf(stderr, "Error: argument %u is not a string\n", i);
            return 0;
        }
    }
    if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: failed to lock mutex\n");
    } else {
        viewer_make_current(shell->viewer);
        if (nArgs == 6) {
            texture = skybox_load_texture_png_6faces(paths[0], paths[1], paths[2], paths[3], paths[4], paths[5]);
        } else {
            texture = skybox_load_texture_hdr_equirect(paths[0], 1024);
        }
        if (!(new = do_skybox(shell, texture))) {
            fprintf(stderr, "Error: failed to create skybox\n");
        } else {
            skybox_free(shell->skybox);
            shell->skybox = new;
        }
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
    }
    for (i = 0; i < nArgs; i++) {
        free(paths[i]);
    }

    ret->type = VOID;
    return new != NULL;
}

int shell_make_skybox_vars(struct DemoShell* shell) {
    return builtin_function_with_data(shell, "skybox", skybox, shell);
}
