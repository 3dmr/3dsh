#include <3dmr/render/lights_buffer_object.h>
#include "builtins.h"
#include "lights.h"

static int ambient_set(const struct Variable* src, void* dest) {
    struct DemoShell* shell = dest;
    if (!vec3_ptr_set(src, shell->ambientLight.color)) return 0;
    lights_buffer_object_update_ambient(&shell->scene.lights, &shell->ambientLight);
    shell->lightsChanged = 1;
    return 1;
}

static const struct PointerInfo ambientPtr = {vec3_ptr_get, ambient_set, NULL, NULL};

int shell_make_lights(struct DemoShell* shell) {
    return builtin_ptr(shell, "ambient", &ambientPtr, shell->ambientLight.color, shell)
        && shell_make_dlights(shell)
        && shell_make_plights(shell)
        && shell_make_slights(shell);
}
