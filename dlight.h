#include "shell.h"

#ifndef TDSH_DLIGHT_H
#define TDSH_DLIGHT_H

int shell_make_dlights(struct DemoShell* shell);

int variable_is_dlight(const struct Variable* var);
struct DirectionalLight* variable_to_dlight(const struct Variable* var);
int variable_set_dlight_index(const struct Variable* var, unsigned int i);
int variable_make_dlight(struct DemoShell* shell, const struct DirectionalLight* dl, struct Variable* dest);

#endif
