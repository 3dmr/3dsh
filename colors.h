#include <3dmr/math/linear_algebra.h>

#ifndef TDSH_COLORS_H
#define TDSH_COLORS_H

enum ShellColor {
    SHELL_COLOR_MULTI,
    NUM_SHELL_COLORS
};

struct ShellColors {
    Vec3 colors[NUM_SHELL_COLORS];
    float hue;
};

struct DemoShell;
void shell_colors_init(struct ShellColors* colors);
int shell_make_color_vars(struct DemoShell* shell);
void shell_update_colors(struct ShellColors* shcolors, double dt);
int variable_is_multi(const struct Variable* var);

#endif
