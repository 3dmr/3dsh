#include <stddef.h>
#include <stdarg.h>
#include <3dmr/math/linear_algebra.h>
#include <3dmr/math/quaternion.h>
#include <3dmr/mesh/sphere_map.h>

#ifndef TDSH_VARIABLES_H
#define TDSH_VARIABLES_H

struct Variable {
    enum VarType {INT, FLOAT, VEC2, VEC3, VEC4, MAT2, MAT3, MAT4, QUATERNION, STRING, POINTER, ARRAY, STRUCT, FUNCTION, VOID /*keep void last*/} type;
    union VarData {
        long dint;
        float dfloat;
        Vec2 dvec2;
        Vec3 dvec3;
        Vec4 dvec4;
        Mat2 dmat2;
        Mat3 dmat3;
        Mat4 dmat4;
        Quaternion dquaternion;
        char* dstring;
        struct PointerData {
            const struct PointerInfo* info;
            void *gdata, *sdata;
        } dpointer;
        struct ArrayData {
            const struct ArrayInfo* info;
            unsigned int *count;
            void* data;
        } darray;
        struct StructData {
            const struct StructInfo* info;
            void* data;
        } dstruct;
        struct FunctionData {
            int (*f)(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret);
            void* data;
            enum FunctionFlags {FUNC_USER = 1, FUNC_DATAREF = 2, FUNC_SIMPLE_COPY = 4} flags;
        } dfunction;
    } data;
};

struct StructInfo {
    const char* name;
    const struct FieldInfo {
        const char* name;
        int (*get)(const void* src, struct Variable* dest);
    } *fields;
    int (*copy)(const struct Variable* src, struct Variable* dest);
    void (*free)(void* data);
};

struct ArrayInfo {
    int (*get)(const void* src, unsigned int i, struct Variable* dest);
    int (*add)(void* dest, const struct Variable* src);
    int (*del)(void* dest, unsigned int i);
    int (*copy)(const struct Variable* src, struct Variable* dest);
    void (*free)(void* data);
};

struct PointerInfo {
    int (*get)(const void* src, struct Variable* dest);
    int (*set)(const struct Variable* src, void* dest);
    int (*copy)(const struct Variable* src, struct Variable* dest);
    void (*free)(struct Variable* v);
};

struct EvalState;

const char* variable_type_string(enum VarType type);

void variable_print(const struct Variable* var, unsigned int indent, int escape);
void variable_print_line(const struct Variable* var);

int variable_array_add(struct Variable* a, const struct Variable* src);
int variable_array_del(struct Variable* a, long i);
int variable_array_get(const struct Variable* a, long i, struct Variable* dest);
int variable_struct_get(const struct Variable* s, const struct FieldInfo* field, struct Variable* dest);
int variable_pointer_get(const struct Variable* src, struct Variable* dest);
int variable_pointer_set(const struct Variable* src, struct Variable* dest);
int variable_copy_dereference(const struct Variable* src, struct Variable* dest);
int variable_move_dereference(const struct Variable* src, struct Variable* dest);
int variable_function_call(const struct Variable* src, struct Variable* dest, const struct Variable* args, unsigned int nArgs, struct EvalState* es);
/* format=(%<type>)*, type=[u][l]d|f|[v|m][2|3|4]|q */
int variable_function_call_v(const struct Variable* func, struct Variable* ret, const char* format, va_list args);
int variable_function_call_l(const struct Variable* func, struct Variable* ret, const char* format, ...);

int variable_to_int(const struct Variable* var, long* dest);
int variable_to_float(const struct Variable* var, float* dest);
char* variable_to_string(const struct Variable* var);
int variable_to_vec2(const struct Variable* var, Vec2 dest);
int variable_to_vec3(const struct Variable* var, Vec3 dest);
int variable_to_vec4(const struct Variable* var, Vec4 dest);
int variable_to_quaternion(const struct Variable* var, Quaternion dest);
int variable_to_mat2(const struct Variable* var, Mat2 dest);
int variable_to_mat3(const struct Variable* var, Mat3 dest);
int variable_to_mat4(const struct Variable* var, Mat4 dest);

int uint_ptr_get(const void* src, struct Variable* dest);
int float_ptr_get(const void* src, struct Variable* dest);
int float_ptr_set(const struct Variable* var, void* dest);
int vec3_ptr_get(const void* src, struct Variable* dest);
int vec3_ptr_set(const struct Variable* var, void* dest);
int vec3_ptr_set_normalize(const struct Variable* var, void* dest);
int quaternion_ptr_get(const void* src, struct Variable* dest);
int string_ptr_get(const void* src, struct Variable* dest);
int string_ptr_set(const struct Variable* src, void* dest);

void variable_make_int(long i, struct Variable* dest);
void variable_make_float(float f, struct Variable* dest);
int variable_make_string(const char* s, size_t n, struct Variable* dest);
void variable_make_ptr(const struct PointerInfo* info, void* gdata, void* sdata, struct Variable* dest);
void variable_make_struct(const struct StructInfo* info, void* data, struct Variable* dest);
void variable_make_array(const struct ArrayInfo* info, void* data, unsigned int* count, struct Variable* dest);
void variable_make_function(int (*f)(void*, const struct Variable*, unsigned int, struct Variable*), void* data, enum FunctionFlags flags, struct Variable* dest);

int variable_copy(const struct Variable* src, struct Variable* dest);
void variable_free(struct Variable* v);

#endif
