#include "builtins.h"
#include "constants.h"

int shell_make_constants(struct DemoShell* shell) {
    return builtin_float(shell, "pi", 3.14159265358979323846264)
        && builtin_vec3(shell, "black", 0, 0, 0)
        && builtin_vec3(shell, "red", 1, 0, 0)
        && builtin_vec3(shell, "green", 0, 1, 0)
        && builtin_vec3(shell, "blue", 0, 0, 1)
        && builtin_vec3(shell, "yellow", 1, 1, 0)
        && builtin_vec3(shell, "magenta", 1, 0, 1)
        && builtin_vec3(shell, "cyan", 0, 1, 1)
        && builtin_vec3(shell, "white", 1, 1, 1)
        && builtin_vec3(shell, "left", -1, 0, 0)
        && builtin_vec3(shell, "right", 1, 0, 0)
        && builtin_vec3(shell, "down", 0, -1, 0)
        && builtin_vec3(shell, "up", 0, 1, 0)
        && builtin_vec3(shell, "forward", 0, 0, -1)
        && builtin_vec3(shell, "backward", 0, 0, 1);
}
