#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3dmr/render/lights_buffer_object.h>
#include "builtins.h"
#include "lights.h"

struct DlightData {
    struct DirectionalLight l;
    unsigned int i;
};

#define vec3n_ptr_get(a, b) vec3_ptr_get(a, b)
#define vec3n_ptr_set(a, b) vec3_ptr_set_normalize(a, b)

static int dlight_ptr_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dpointer.sdata)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}
static void dlight_ptr_free(struct Variable* var) {
    shell_decref(var->data.dpointer.sdata);
}

#define DLIGHT_MEMBER(type, name) \
static int dlight_##name##_get(const void* src, struct Variable* dest) { \
    const struct ShellRefCount* r = src; \
    struct DlightData* ld = r->data; \
    return type##_ptr_get(ld->l.name, dest); \
} \
static int dlight_##name##_set(const struct Variable* src, void* dest) { \
    struct ShellRefCount* r = dest; \
    struct DlightData* ld = r->data; \
    if (!type##_ptr_set(src, ld->l.name)) return 0; \
    if (ld->i < MAX_DIRECTIONAL_LIGHTS) { \
        lights_buffer_object_update_dlight(&r->shell->scene.lights, &ld->l, ld->i); \
        r->shell->lightsChanged = 1; \
    } \
    return 1; \
} \
static struct PointerInfo dlight##_##name = {dlight_##name##_get, dlight_##name##_set, dlight_ptr_copy, dlight_ptr_free}; \
static int dlight_ptr_##name(const void* src, struct Variable* dest) { \
    struct ShellRefCount* r = (void*)src; \
    if (!shell_incref(r)) return 0; \
    variable_make_ptr(&dlight##_##name, r, r, dest); \
    return 1; \
}

DLIGHT_MEMBER(vec3, color)
DLIGHT_MEMBER(vec3n, direction)

static int dlight_id_get(const void* src, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    struct DlightData* ld = r->data;
    variable_make_int((ld->i < MAX_DIRECTIONAL_LIGHTS) ? (long)ld->i : -1L, dest);
    return 1;
}
static int dlight_id_set(const struct Variable* src, void* dest) {
    struct ShellRefCount *r = dest, *old = NULL;
    struct DlightData* ld = r->data;
    long id;
    unsigned int i;
    if (!variable_to_int(src, &id)) return 0;
    if (id < 0) {
        if (ld->i >= MAX_DIRECTIONAL_LIGHTS) return 0;
        if ((old = r->shell->rdlight[ld->i])) {
            if (--r->shell->numDirectionalLights > ld->i) {
                struct DlightData* last = r->shell->rdlight[r->shell->numDirectionalLights]->data;
                r->shell->rdlight[ld->i] = r->shell->rdlight[r->shell->numDirectionalLights];
                r->shell->rdlight[r->shell->numDirectionalLights] = NULL;
                last->i = ld->i;
                lights_buffer_object_update_dlight(&r->shell->scene.lights, &last->l, last->i);
            }
            ld->i = -1;
            lights_buffer_object_update_ndlight(&r->shell->scene.lights, r->shell->numDirectionalLights);
            r->shell->lightsChanged = 1;
        }
    } else {
        if (id > MAX_DIRECTIONAL_LIGHTS) return 0;
        i = id;
        if (i >= r->shell->numDirectionalLights) return 0;
        if (ld->i >= MAX_DIRECTIONAL_LIGHTS) {
            if (!shell_incref(r)) return 0;
        }
        if (ld->i < MAX_DIRECTIONAL_LIGHTS) {
            struct DlightData* other = r->shell->rdlight[i]->data;
            r->shell->rdlight[ld->i] = r->shell->rdlight[i];
            other->i = ld->i;
            lights_buffer_object_update_dlight(&r->shell->scene.lights, &other->l, other->i);
        } else {
            old = r->shell->rdlight[i];
        }
        r->shell->rdlight[i] = r;
        ld->i = i;
        lights_buffer_object_update_dlight(&r->shell->scene.lights, &ld->l, ld->i);
        r->shell->lightsChanged = 1;
    }
    if (old) shell_decref(old);
    return 1;
}
static struct PointerInfo dlight_id = {dlight_id_get, dlight_id_set, dlight_ptr_copy, dlight_ptr_free};
static int dlight_ptr_id(const void* src, struct Variable* dest) {
    struct ShellRefCount* r = (void*)src;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(&dlight_id, r, r, dest);
    return 1;
}

static const struct FieldInfo structDlightFields[] = {
    {"color", dlight_ptr_color},
    {"direction", dlight_ptr_direction},
    {"id", dlight_ptr_id},
    {0}
};

static const struct StructInfo structDlight;

static int dlight_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dstruct.data)) return 0;
    variable_make_struct(&structDlight, src->data.dstruct.data, dest);
    return 1;
}

static void dlight_free(void* data) {
    shell_decref(data);
}

static const struct StructInfo structDlight = {"DLight", structDlightFields, dlight_copy, dlight_free};

int variable_is_dlight(const struct Variable* var) {
    return var->type == STRUCT && var->data.dstruct.info == &structDlight;
}

struct DirectionalLight* variable_to_dlight(const struct Variable* var) {
    if (variable_is_dlight(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct DlightData* ld = r->data;
        return &ld->l;
    }
    return NULL;
}

int variable_set_dlight_index(const struct Variable* var, unsigned int i) {
    if (variable_is_dlight(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        struct DlightData* ld = r->data;
        ld->i = i;
        return 1;
    }
    return 0;
}

static int dlight_add(void* dest, const struct Variable* src) {
    struct Variable tmp;
    struct DemoShell* shell = dest;
    struct ShellRefCount* r;
    int ok;
    if (shell->numDirectionalLights >= MAX_DIRECTIONAL_LIGHTS
     || !variable_copy_dereference(src, &tmp)) {
        return 0;
    }
    if ((ok = variable_is_dlight(&tmp) && ((struct DlightData*)(r = tmp.data.dstruct.data)->data)->i == ((unsigned int)-1))) {
        if (shell->rdlight[shell->numDirectionalLights]) {
            struct DlightData* ld = shell->rdlight[shell->numDirectionalLights]->data;
            ld->i = -1;
            shell_decref(shell->rdlight[shell->numDirectionalLights]);
        }
        shell->rdlight[shell->numDirectionalLights] = r;
        variable_set_dlight_index(&tmp, shell->numDirectionalLights);
        lights_buffer_object_update_dlight(&shell->scene.lights, variable_to_dlight(&tmp), shell->numDirectionalLights);
        lights_buffer_object_update_ndlight(&shell->scene.lights, ++shell->numDirectionalLights);
        shell->lightsChanged = 1;
    } else {
        variable_free(&tmp);
    }
    return ok;
}

static int dlight_del(void* dest, unsigned int i) {
    struct DemoShell* shell = dest;

    if (i >= shell->numDirectionalLights) return 0;
    --(shell->numDirectionalLights);
    if (shell->rdlight[i]) {
        struct DlightData* ld = shell->rdlight[i]->data;
        ld->i = -1;
        shell_decref(shell->rdlight[i]);
        shell->rdlight[i] = NULL;
    }
    if (shell->numDirectionalLights) {
        shell->rdlight[i] = shell->rdlight[shell->numDirectionalLights];
        shell->rdlight[shell->numDirectionalLights] = NULL;
        if (shell->rdlight[i]) {
            struct DlightData* ld = shell->rdlight[i]->data;
            ld->i = i;
            lights_buffer_object_update_dlight(&shell->scene.lights, &ld->l, i);
        }
    }
    lights_buffer_object_update_ndlight(&shell->scene.lights, shell->numDirectionalLights);
    shell->lightsChanged = 1;
    return 1;
}

static int dlight_get(const void* src, unsigned int i, struct Variable* dest) {
    const struct DemoShell* shell = src;
    struct ShellRefCount* r;

    if (i >= shell->numDirectionalLights || !(r = shell->rdlight[i]) || !shell_incref(r)) {
        return 0;
    }
    variable_make_struct(&structDlight, r, dest);
    return 1;
}

static void dlight_destroy(struct DemoShell* shell, void* l) {
    free(l);
}

static int dlight(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct DlightData* ld;
    struct DirectionalLight* l;
    struct ShellRefCount* r;

    if (nArgs != 2) {
        fprintf(stderr, "Error: dlight() needs two vec3 arguments\n");
        return 0;
    }
    if (!(ld = malloc(sizeof(*ld)))) {
        fprintf(stderr, "Error: failed to allocate dlight\n");
        return 0;
    }
    l = &ld->l;
    if (!variable_to_vec3(args, l->color) || !variable_to_vec3(args + 1, l->direction)) {
        fprintf(stderr, "Error: dlight() needs two vec3 arguments\n");
        free(ld);
        return 0;
    }
    normalize3(l->direction);
    ld->i = -1;
    if (!(r = shell_new_refcount(shell, dlight_destroy, ld))) {
        free(ld);
        return 0;
    }
    variable_make_struct(&structDlight, r, ret);
    return 1;
}

static struct ArrayInfo arrayDlights = {dlight_get, dlight_add, dlight_del, NULL, NULL};

int shell_make_dlights(struct DemoShell* shell) {
    struct Variable* dest;

    if (!(dest = shell_add_variable(shell, "dlights"))) {
        return 0;
    }
    variable_make_array(&arrayDlights, shell, &shell->numDirectionalLights, dest);

    return builtin_function_with_data(shell, "dlight", dlight, shell);
}

int variable_make_dlight(struct DemoShell* shell, const struct DirectionalLight* dl, struct Variable* dest) {
    struct DlightData* ld;
    struct DirectionalLight* l;
    struct ShellRefCount* r;

    if (!(ld = malloc(sizeof(*ld)))) {
        return 0;
    }
    l = &ld->l;
    *l = *dl;
    ld->i = -1;
    if (!(r = shell_new_refcount(shell, dlight_destroy, ld))) {
        free(ld);
        return 0;
    }
    variable_make_struct(&structDlight, r, dest);
    return 1;
}
