#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <3dmr/mesh/mesh.h>
#include <3dmr/render/viewer.h>
#include <3dmr/scene/frame.h>
#include "builtins.h"
#include "material.h"
#include "mesh.h"
#include "node.h"
#include "shell.h"
#include "vertex_array.h"
#include "camera.h"
#include "dlight.h"
#include "plight.h"
#include "slight.h"
#include "render.h"

static void node_destroy_(struct DemoShell* shell, void* d) {
    struct Node* n = d;
    struct NodeVarData* nd = (void*)(n + 1);
    unsigned int i;

    if (!d) return;
    switch (n->type) {
        case NODE_GEOMETRY:
            variable_free(&nd->data.geom.material);
            variable_free(&nd->data.geom.vertexArray);
            break;
        case NODE_DLIGHT:
        case NODE_PLIGHT:
        case NODE_SLIGHT:
            variable_free(&nd->data.light);
            break;
        case NODE_CAMERA:
            variable_set_camera_node(&nd->data.camera, NULL);
            variable_free(&nd->data.camera);
            break;
        case NODE_EMPTY:;
        case NODE_BONE:;
    }
    for (i = 0; i < n->nbChildren; i++) {
        struct Node* c = n->children[i];
        struct NodeVarData* cd = (void*)(c + 1);
        shell_decref(cd->self);
    }
    free(n->children);
    free(d);
}

static void warn_missing_flags(unsigned int flags, unsigned int reqFlags) {
    unsigned int missingFlags = (flags ^ reqFlags) & reqFlags;
    if (missingFlags & MESH_NORMALS) {
        fprintf(stderr, "Error: this material requires normals, but the vertex array has not\n");
    } else if (missingFlags & MESH_TEXCOORDS) {
        fprintf(stderr, "Error: this material requires texcoords, but the vertex array has not\n");
    } else if (missingFlags & MESH_TANGENTS) {
        fprintf(stderr, "Error: this material requires tangents, but the vertex array has not\n");
    } else {
        fprintf(stderr, "Error: vertex array does not meet material requirements, missing flags: 0x%X\n", missingFlags);
    }
}

static struct ShellRefCount* shell_new_node(struct DemoShell* shell) {
    struct ShellRefCount* r;
    struct Node* n = NULL;
    struct NodeVarData* d;

    if (!(r = shell_new_refcount(shell, node_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return 0;
    }
    if (!(n = malloc(sizeof(*n) + sizeof(struct NodeVarData)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return 0;
    }
    node_init(n);
    r->data = n;
    d = (void*)(n + 1);
    d->self = r;
    return r;
}

static void set_angle(struct Node* n, Vec3 d) {
    Vec3 axis, down = {0, -1, 0};
    float angle;
    compute_rotation(down, d, axis, &angle);
    if (!axis[0] && !axis[1] && !axis[2] && angle) {
        axis[0] = 1;
        axis[1] = 0;
        axis[2] = 0;
    }
    quaternion_set_axis_angle(n->orientation, axis, angle);
    n->changedFlags |= ORIENTATION_CHANGED;
}

static struct ShellRefCount* shell_new_node_dlight(struct DemoShell* shell, const struct Variable* dLight) {
    struct Variable tmp;
    struct ShellRefCount* r;
    struct Node* n = NULL;
    struct NodeVarData* d;

    if (!(r = shell_new_node(shell))) {
        return 0;
    }
    n = r->data;
    d = (void*)(n + 1);
    if (!variable_copy_dereference(dLight, &tmp)) {
        fprintf(stderr, "Error: failed to copy light argument\n");
        shell_decref(r);
        return 0;
    }
    d->data.light = tmp;
    if (!variable_is_dlight(&tmp)) {
        fprintf(stderr, "Error: expected struct DLight\n");
        variable_free(&tmp);
        shell_decref(r);
        return 0;
    }
    node_set_dlight(n, variable_to_dlight(&d->data.light));
    set_angle(n, n->data.dlight->direction);
    return r;
}

static struct ShellRefCount* shell_new_node_plight(struct DemoShell* shell, const struct Variable* pLight) {
    struct Variable tmp;
    struct ShellRefCount* r;
    struct Node* n = NULL;
    struct NodeVarData* d;

    if (!(r = shell_new_node(shell))) {
        return 0;
    }
    n = r->data;
    d = (void*)(n + 1);
    if (!variable_copy_dereference(pLight, &tmp)) {
        fprintf(stderr, "Error: failed to copy light argument\n");
        shell_decref(r);
        return 0;
    }
    d->data.light = tmp;
    if (!variable_is_plight(&tmp)) {
        fprintf(stderr, "Error: expected struct PLight\n");
        variable_free(&tmp);
        shell_decref(r);
        return 0;
    }
    node_set_plight(n, variable_to_plight(&d->data.light));
    node_set_pos(n, n->data.plight->position);
    return r;
}

static struct ShellRefCount* shell_new_node_slight(struct DemoShell* shell, const struct Variable* sLight) {
    struct Variable tmp;
    struct ShellRefCount* r;
    struct Node* n = NULL;
    struct NodeVarData* d;

    if (!(r = shell_new_node(shell))) {
        return 0;
    }
    n = r->data;
    d = (void*)(n + 1);
    if (!variable_copy_dereference(sLight, &tmp)) {
        fprintf(stderr, "Error: failed to copy light argument\n");
        shell_decref(r);
        return 0;
    }
    d->data.light = tmp;
    if (!variable_is_slight(&tmp)) {
        fprintf(stderr, "Error: expected struct SLight\n");
        variable_free(&tmp);
        shell_decref(r);
        return 0;
    }
    node_set_slight(n, variable_to_slight(&d->data.light));
    node_set_pos(n, n->data.slight->position);
    set_angle(n, n->data.slight->direction);
    return r;
}

static struct ShellRefCount* shell_new_node_camera(struct DemoShell* shell, const struct Variable* camera) {
    struct Variable tmp;
    struct ShellRefCount* r;
    struct Node* n = NULL;
    struct NodeVarData* d;

    if (!(r = shell_new_node(shell))) {
        return NULL;
    }
    n = r->data;
    d = (void*)(n + 1);
    if (!variable_copy_dereference(camera, &tmp)) {
        fprintf(stderr, "Error: failed to copy camera argument\n");
        shell_decref(r);
        return NULL;
    }
    d->data.camera = tmp;
    if (!variable_is_camera(&tmp)) {
        fprintf(stderr, "Error: expected struct Camera\n");
        variable_free(&tmp);
        shell_decref(r);
        return NULL;
    }
    node_set_camera(n, variable_to_camera(&d->data.camera));
    if (!variable_set_camera_node(&d->data.camera, n)) {
        fprintf(stderr, "Error: failed to assign camera to node, is camera already assigned?\n");
        shell_decref(r);
        return NULL;
    }
    return r;
}

static struct ShellRefCount* shell_new_node_geometry(struct DemoShell* shell, const struct Variable* aMesh, const struct Variable* aMat) {
    struct Variable tmp;
    struct ShellRefCount* r;
    struct Node* n = NULL;
    struct NodeVarData* d;
    unsigned int flags, reqFlags;

    if (!(r = shell_new_node(shell))) {
        return 0;
    }
    n = r->data;
    d = (void*)(n + 1);
    if (!variable_copy_dereference(aMat, &tmp)) {
        fprintf(stderr, "Error: failed to copy material argument\n");
        shell_decref(r);
        return 0;
    }
    d->data.geom.material = tmp;
    if (!variable_is_material(&tmp)) {
        fprintf(stderr, "Error: expected struct Material\n");
        variable_free(&tmp);
        shell_decref(r);
        return 0;
    }
    reqFlags = variable_to_material_data(&d->data.geom.material)->requiredFlags;
    if (!variable_copy_dereference(aMesh, &tmp)) {
        fprintf(stderr, "Error: failed to copy mesh argument\n");
        variable_free(&d->data.geom.material);
        shell_decref(r);
        return 0;
    }
    if (variable_is_mesh(&tmp)) {
        struct Mesh* mesh = variable_to_mesh(&tmp);
        int ok;
        if ((reqFlags & MESH_TANGENTS) && !(mesh->flags & MESH_TANGENTS)) {
            mesh_compute_tangents(mesh);
        }
        ok = vertex_array_from_mesh(shell, mesh, &d->data.geom.vertexArray);
        variable_free(&tmp);
        if (!ok) {
            fprintf(stderr, "Error: failed to create vertex array from mesh\n");
            shell_decref(r);
            return 0;
        }
    } else if (variable_is_vertex_array(&tmp)) {
        d->data.geom.vertexArray = tmp;
    } else {
        fprintf(stderr, "Error: expected struct Mesh or struct VertexArray\n");
        variable_free(&tmp);
        shell_decref(r);
        return 0;
    }
    flags = variable_to_vertex_array(&d->data.geom.vertexArray)->flags;
    if ((flags & reqFlags) != reqFlags) {
        warn_missing_flags(flags, reqFlags);
        shell_decref(r);
        return 0;
    }
    d->data.geom.geometry.vertexArray = variable_to_vertex_array(&d->data.geom.vertexArray);
    d->data.geom.geometry.material = variable_to_material(&d->data.geom.material);
    node_set_geometry(n, &d->data.geom.geometry);
    return r;
}

static struct ShellRefCount* node_deep_copy(const struct ShellRefCount* src) {
    struct DemoShell* shell = src->shell;
    struct ShellRefCount* r;
    struct Node *n = NULL, *sn = src->data;
    struct NodeVarData *d, *sd = (void*)(sn + 1);

    if (!(r = shell_new_refcount(shell, node_destroy_, NULL))) {
        fprintf(stderr, "Error: failed to create refcount\n");
        return 0;
    }
    if (!(n = malloc(sizeof(*n) + sizeof(struct NodeVarData)))) {
        fprintf(stderr, "Error: memory allocation failed\n");
        shell_decref(r);
        return 0;
    }
    node_init(n);
    r->data = n;
    d = (void*)(n + 1);
    switch (sn->type) {
        case NODE_EMPTY:
        case NODE_BONE:
            break;
        case NODE_GEOMETRY:
            if (!variable_copy(&sd->data.geom.vertexArray, &d->data.geom.vertexArray)
             || !variable_copy(&sd->data.geom.material, &d->data.geom.material)) {
                fprintf(stderr, "Error: failed to copy node variable\n");
                shell_decref(r);
                return 0;
            }
            d->data.geom.geometry.vertexArray = variable_to_vertex_array(&d->data.geom.vertexArray);
            d->data.geom.geometry.material = variable_to_material(&d->data.geom.material);
            node_set_geometry(n, &d->data.geom.geometry);
            break;
        case NODE_DLIGHT:
            if (!variable_copy(&sd->data.light, &d->data.light)) {
                fprintf(stderr, "Error: failed to copy node variable\n");
                shell_decref(r);
                return 0;
            }
            node_set_dlight(n, variable_to_dlight(&d->data.light));
            break;
        case NODE_PLIGHT:
            if (!variable_copy(&sd->data.light, &d->data.light)) {
                fprintf(stderr, "Error: failed to copy node variable\n");
                shell_decref(r);
                return 0;
            }
            node_set_plight(n, variable_to_plight(&d->data.light));
            break;
        case NODE_SLIGHT:
            if (!variable_copy(&sd->data.light, &d->data.light)) {
                fprintf(stderr, "Error: failed to copy node variable\n");
                shell_decref(r);
                return 0;
            }
            node_set_slight(n, variable_to_slight(&d->data.light));
            break;
        case NODE_CAMERA:
            if (!variable_copy(&sd->data.camera, &d->data.camera)) {
                fprintf(stderr, "Error: failed to copy node variable\n");
                shell_decref(r);
                return 0;
            }
            node_set_camera(n, variable_to_camera(&d->data.camera));
            break;
        default:
            fprintf(stderr, "Error: copy of node with type %u is not supported\n", (unsigned int)(sn->type));
            shell_decref(r);
            return 0;
    }
    d->self = r;
    return r;
}

int node_refptr_copy(const struct Variable* src, struct Variable* dest) {
    struct ShellRefCount* r = src->data.dpointer.sdata;
    if (!shell_incref(r)) return 0;
    variable_make_ptr(src->data.dpointer.info, src->data.dpointer.gdata, src->data.dpointer.sdata, dest);
    return 1;
}

void node_refptr_free(struct Variable* var) {
    shell_decref(var->data.dpointer.sdata);
}

static int node_position_set(const struct Variable* src, void* dest) {
    struct Node* n = ((const struct ShellRefCount*)dest)->data;
    if (!variable_to_vec3(src, n->position)) return 0;
    n->changedFlags |= POSITION_CHANGED;
    return 1;
}

static const struct PointerInfo nodePositionPtr = {vec3_ptr_get, node_position_set, node_refptr_copy, node_refptr_free};

static int node_position(const void* src, struct Variable* dest) {
    const struct Node* n = ((const struct ShellRefCount*)src)->data;
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodePositionPtr, (void*)n->position, (void*)src, dest);
    return 1;
}

static int node_orientation_set(const struct Variable* src, void* dest) {
    struct Node* n = ((const struct ShellRefCount*)dest)->data;
    if (!variable_to_quaternion(src, n->orientation)) return 0;
    n->changedFlags |= ORIENTATION_CHANGED;
    return 1;
}

static const struct PointerInfo nodeOrientationPtr = {quaternion_ptr_get, node_orientation_set, node_refptr_copy, node_refptr_free};

static int node_orientation(const void* src, struct Variable* dest) {
    const struct Node* n = ((const struct ShellRefCount*)src)->data;
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodeOrientationPtr, (void*)n->orientation, (void*)src, dest);
    return 1;
}

static int node_scale_set(const struct Variable* src, void* dest) {
    struct Node* n = ((const struct ShellRefCount*)dest)->data;
    if (!variable_to_vec3(src, n->scale)) return 0;
    n->changedFlags |= SCALE_CHANGED;
    return 1;
}

static const struct PointerInfo nodeScalePtr = {vec3_ptr_get, node_scale_set, node_refptr_copy, node_refptr_free};

static int node_scale(const void* src, struct Variable* dest) {
    const struct Node* n = ((const struct ShellRefCount*)src)->data;
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodeScalePtr, (void*)n->scale, (void*)src, dest);
    return 1;
}

static int node_draw_get(const void* src, struct Variable* dest) {
    const char* s;
    int v = *(int*)src;
    if (v > 0) {
        s = "always";
    } else if (v < 0) {
        s = "never";
    } else {
        s = "auto";
    }
    return variable_make_string(s, strlen(s), dest);
}

static int node_draw_set(const struct Variable* src, void* dest) {
    struct Node* n = ((const struct ShellRefCount*)dest)->data;
    char* s;
    if (!(s = variable_to_string(src))) return 0;
    if (!strcmp(s, "always")) {
        n->alwaysDraw = 1;
    } else if (!strcmp(s, "never")) {
        n->alwaysDraw = -1;
    } else if (!strcmp(s, "auto")) {
        n->alwaysDraw = 0;
    } else {
        free(s);
        return 0;
    }
    free(s);
    ((const struct ShellRefCount*)dest)->shell->graphChanged = 1;
    return 1;
}

static const struct PointerInfo nodeDrawPtr = {node_draw_get, node_draw_set, node_refptr_copy, node_refptr_free};

static int node_draw(const void* src, struct Variable* dest) {
    const struct Node* n = ((const struct ShellRefCount*)src)->data;
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodeDrawPtr, (void*)&n->alwaysDraw, (void*)src, dest);
    return 1;
}

static int varmem_get(const void* src, struct Variable* dest) {
    return variable_copy(src, dest);
}

static int node_va_set(const struct Variable* src, void* dest) {
    struct Variable tmp;
    const struct Node* n = ((struct ShellRefCount*)dest)->data;
    struct NodeVarData* nd = (void*)(n + 1);
    struct VertexArray* va;
    unsigned int reqFlags = variable_to_material_data(&nd->data.geom.material)->requiredFlags;

    if (!variable_copy_dereference(src, &tmp)) {
        return 0;
    }
    if (variable_is_mesh(&tmp)) {
        struct Variable va;
        struct Mesh* mesh = variable_to_mesh(&tmp);
        if ((reqFlags & MESH_TANGENTS) && !(mesh->flags & MESH_TANGENTS)) {
            mesh_compute_tangents(mesh);
        }
        if (vertex_array_from_mesh(nd->self->shell, mesh, &va)) {
            variable_free(&tmp);
            tmp = va;
        }
    }
    if (!(va = variable_to_vertex_array(&tmp))) {
        variable_free(&tmp);
        return 0;
    }
    if ((va->flags & reqFlags) != reqFlags) {
        warn_missing_flags(va->flags, reqFlags);
        variable_free(&tmp);
        return 0;
    }
    nd->data.geom.geometry.vertexArray = variable_to_vertex_array(&tmp);
    variable_free(&nd->data.geom.vertexArray);
    nd->data.geom.vertexArray = tmp;
    return 1;
}

static const struct PointerInfo nodeVertexArrayPtr = {varmem_get, node_va_set, node_refptr_copy, node_refptr_free};

static int node_vertex_array(const void* src, struct Variable* dest) {
    const struct Node* n = ((struct ShellRefCount*)src)->data;
    struct NodeVarData* nd = (void*)(n + 1);
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodeVertexArrayPtr, &nd->data.geom.vertexArray, (void*)src, dest);
    return 1;
}

static int node_material_set(const struct Variable* src, void* dest) {
    struct Variable tmp;
    const struct Node* n = ((struct ShellRefCount*)dest)->data;
    struct NodeVarData* nd = (void*)(n + 1);

    if (!variable_copy_dereference(src, &tmp)) {
        return 0;
    }
    if (!variable_is_material(&tmp)) {
        variable_free(&tmp);
        return 0;
    }
    nd->data.geom.geometry.material = variable_to_material(&tmp);
    variable_free(&nd->data.geom.material);
    nd->data.geom.material = tmp;
    return 1;
}

static const struct PointerInfo nodeMaterialPtr = {varmem_get, node_material_set, node_refptr_copy, node_refptr_free};

static int node_material(const void* src, struct Variable* dest) {
    const struct Node* n = ((struct ShellRefCount*)src)->data;
    struct NodeVarData* nd = (void*)(n + 1);
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodeMaterialPtr, &nd->data.geom.material, (void*)src, dest);
    return 1;
}

static const struct PointerInfo nodeLightPtr = {varmem_get, NULL, node_refptr_copy, node_refptr_free};

static int node_light(const void* src, struct Variable* dest) {
    const struct Node* n = ((struct ShellRefCount*)src)->data;
    struct NodeVarData* nd = (void*)(n + 1);
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_ptr(&nodeLightPtr, &nd->data.light, (void*)src, dest);
    return 1;
}

static int node_camera(const void* src, struct Variable* dest) {
    const struct Node* n = ((struct ShellRefCount*)src)->data;
    struct NodeVarData* nd = (void*)(n + 1);
    return variable_copy(&nd->data.camera, dest);
}

static int node_children_add(void* dest, const struct Variable* src) {
    struct Variable tmp;
    struct ShellRefCount* r = dest;
    struct Node *n = r->data, *c;

    if (!variable_copy_dereference(src, &tmp)) {
        return 0;
    }
    if (!variable_is_node(&tmp)) {
        variable_free(&tmp);
        return 0;
    }
    c = variable_to_node(&tmp);
    if (!node_add_child(n, c)) {
        variable_free(&tmp);
        return 0;
    }
    return 1;
}

static int node_children_del(void* dest, unsigned int i) {
    struct ShellRefCount* r = dest;
    struct Node *n = r->data, *c;
    struct NodeVarData* cd;

    if (i >= n->nbChildren) return 0;
    c = n->children[i];
    n->children[i] = n->children[--(n->nbChildren)];
    r->shell->graphChanged = 1;
    cd = (void*)(c + 1);
    shell_decref(cd->self);
    return 1;
}

static const struct StructInfo structNode, structGeomNode, structLightNode, structCameraNode;

const struct StructInfo* node_struct_type(enum NodeType type) {
    switch (type) {
        case NODE_EMPTY:    return &structNode;
        case NODE_BONE:     return &structNode;
        case NODE_GEOMETRY: return &structGeomNode;
        case NODE_DLIGHT:   return &structLightNode;
        case NODE_PLIGHT:   return &structLightNode;
        case NODE_SLIGHT:   return &structLightNode;
        case NODE_CAMERA:   return &structCameraNode;
    }
    return NULL;
}

static int node_children_get(const void* src, unsigned int i, struct Variable* dest) {
    const struct ShellRefCount* r = src;
    const struct Node* n = r->data;
    struct NodeVarData* nd;

    if (i >= n->nbChildren) {
        return 0;
    }
    n = n->children[i];
    nd = (void*)(n + 1);
    if (!shell_incref(nd->self)) return 0;
    variable_make_struct(node_struct_type(n->type), nd->self, dest);
    return 1;
}

int node_children_copy(const struct Variable* src, struct Variable* dest) {
    struct ShellRefCount* r = src->data.darray.data;
    if (!shell_incref(r)) return 0;
    variable_make_array(src->data.darray.info, r, src->data.darray.count, dest);
    return 1;
}

void node_children_free(void* data) {
    shell_decref(data);
}

static const struct ArrayInfo arrayNodeChildren = {node_children_get, node_children_add, node_children_del, node_children_copy, node_children_free};

static int node_children(const void* src, struct Variable* dest) {
    struct Node* n = ((struct ShellRefCount*)src)->data;
    if (!shell_incref((struct ShellRefCount*)src)) return 0;
    variable_make_array(&arrayNodeChildren, (void*)src, &n->nbChildren, dest);
    return 1;
}

#define NODE_BASE \
    {"position", node_position}, \
    {"orientation", node_orientation}, \
    {"scale", node_scale}, \
    {"children", node_children}, \
    {"draw", node_draw}, \

static const struct FieldInfo structNodeFields[] = {
    NODE_BASE
    {0}
};

static const struct FieldInfo structGeomNodeFields[] = {
    NODE_BASE
    {"vertexArray", node_vertex_array},
    {"material", node_material},
    {0}
};

static const struct FieldInfo structLightNodeFields[] = {
    NODE_BASE
    {"light", node_light},
    {0}
};

static const struct FieldInfo structCameraNodeFields[] = {
    NODE_BASE
    {"camera", node_camera},
    {0}
};

static int node_struct_copy(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(src->data.dstruct.info, data, dest);
    return 1;
}

static void node_struct_free(void* data) {
    shell_decref(data);
}

static const struct StructInfo structNode = {"Node", structNodeFields, node_struct_copy, node_struct_free};
static const struct StructInfo structGeomNode = {"GeometryNode", structGeomNodeFields, node_struct_copy, node_struct_free};
static const struct StructInfo structLightNode = {"LightNode", structLightNodeFields, node_struct_copy, node_struct_free};
static const struct StructInfo structCameraNode = {"CameraNode", structCameraNodeFields, node_struct_copy, node_struct_free};

static int node(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct ShellRefCount* r;

    if (nArgs == 0) {
        if (!(r = shell_new_node(shell))) {
            return 0;
        }
        variable_make_struct(&structNode, r, ret);
    } else if (nArgs == 1) {
        struct Variable tmp;
        int ptr = variable_move_dereference(args, &tmp);
        if (variable_is_dlight(&tmp)) {
            r = shell_new_node_dlight(shell, &tmp);
        } else if (variable_is_plight(&tmp)) {
            r = shell_new_node_plight(shell, &tmp);
        } else if (variable_is_slight(&tmp)) {
            r = shell_new_node_slight(shell, &tmp);
        } else if (variable_is_camera(&tmp)) {
            r = shell_new_node_camera(shell, &tmp);
        } else {
            fprintf(stderr, "Error: cannot create node from argument\n");
            r = NULL;
        }
        if (ptr) variable_free(&tmp);
        if (!r) return 0;
        variable_make_struct(node_struct_type(((struct Node*)r->data)->type), r, ret);
    } else if (nArgs == 2) {
        if (!(r = shell_new_node_geometry(shell, args, args + 1))) {
            return 0;
        }
        variable_make_struct(&structGeomNode, r, ret);
    } else {
        fprintf(stderr, "Error: node() wrong number of arguments\n");
        return 0;
    }

    return 1;
}

static int grid(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct ShellRefCount *root, *template, *r;
    Vec3 position;
    Vec2 spacing;
    long x, y, nx, ny;
    int ok = 1;

    if (nArgs != 5) {
        fprintf(stderr, "Error: grid() needs 5 arguments\n");
        return 0;
    }
    if (!variable_to_int(args, &nx) || !variable_to_int(args + 1, &ny) || !variable_to_vec2(args + 2, spacing)) {
        fprintf(stderr, "Error: invalid argument in grid()\n");
        return 0;
    }
    if (!(root = shell_new_node(shell))) {
        return 0;
    }
    if (!(template = shell_new_node_geometry(shell, args + 3, args + 4))) {
        shell_decref(root);
        return 0;
    }
    position[0] = -((float)(nx - 1) / 2.0f);
    position[2] = 0;
    for (x = 0; ok && x < nx; x++) {
        position[1] = -((float)(ny - 1) / 2.0f);
        for (y = 0; ok && y < ny; y++) {
            if (!(r = node_deep_copy(template))) {
                ok = 0;
            } else if (!node_add_child(root->data, r->data)) {
                shell_decref(r);
                ok = 0;
            } else {
                node_translate(r->data, position);
                position[1] += spacing[1];
            }
        }
        position[0] += spacing[0];
    }
    shell_decref(template);
    if (!ok) shell_decref(root);
    variable_make_struct(&structNode, root, ret);
    return ok;
}

static void scene_destroy(struct DemoShell* shell, void* d) {
    struct Node* n = d;
    unsigned int i;

    for (i = 0; i < n->nbChildren; i++) {
        struct Node* c = n->children[i];
        struct NodeVarData* cd = (void*)(c + 1);
        shell_decref(cd->self);
    }
    n->nbChildren = 0;
    free(n->children);
    n->children = NULL;
}

static int scene_struct_copy(const struct Variable* src, struct Variable* dest) {
    if (!shell_incref(src->data.dstruct.data)) return 0;
    variable_make_struct(src->data.dstruct.info, src->data.dstruct.data, dest);
    return 1;
}

static void scene_struct_free(void* d) {
    shell_decref(d);
}

static const struct StructInfo structScene = {"Node", structNodeFields, scene_struct_copy, scene_struct_free};

static int frame(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct Node* f;
    int ok = 0;

    if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: failed to lock mutex\n");
        return 0;
    }
    viewer_make_current(shell->viewer);
    if (!(f = make_frame())) {
        fprintf(stderr, "Error: failed to create frame\n");
    } else if (!variable_make_node(shell, f, ret)) {
        fprintf(stderr, "Error: failed to make frame node\n");
        free_frame(f);
    } else {
        nodes_free(f, NULL);
        free(f);
        ok = 1;
    }
    viewer_make_current(NULL);
    pthread_mutex_unlock(&shell->swapMutex);
    return ok;
}

int shell_make_node_functions(struct DemoShell* shell) {
    struct ShellRefCount* r;
    if ((r = shell_new_refcount(shell, scene_destroy, &shell->scene.root))
        && builtin_function_with_data(shell, "node" , node, shell)
        && builtin_function_with_data(shell, "grid" , grid, shell)
        && builtin_struct(shell, "scene", &structScene, r)
        && builtin_function_with_data(shell, "frame", frame, shell)) {
        return 1;
    }
    shell_decref(r);
    return 0;
}

int variable_is_node(const struct Variable* var) {
    return var->type == STRUCT &&
        (   var->data.dstruct.info == &structNode
         || var->data.dstruct.info == &structGeomNode
         || var->data.dstruct.info == &structLightNode
         || var->data.dstruct.info == &structCameraNode);
}

struct Node* variable_to_node(const struct Variable* var) {
    if (variable_is_node(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}

int variable_is_scene(const struct Variable* var) {
    return var->type == STRUCT &&var->data.dstruct.info == &structScene;
}

struct Node* variable_to_scene(const struct Variable* var) {
    if (variable_is_scene(var)) {
        struct ShellRefCount* r = var->data.dstruct.data;
        return r->data;
    }
    return NULL;
}

struct NodeVarData* node_get_var_data(const struct Node* node) {
    return (void*)(node + 1);
}

int variable_make_node(struct DemoShell* shell, struct Node* node, struct Variable* dest) {
    struct ShellRefCount *r, *parent = NULL, *root = NULL;
    struct Node *cur, *next = node, *new;
    struct NodeVarData *d;
    const struct StructInfo* sinfo;

    for (cur = node; next; cur = next) {
        if (!(r = shell_new_node(shell))
         || (parent && !node_add_child(parent->data, r->data))) {
            if (r) shell_decref(r);
            if (root) shell_decref(root);
            return 0;
        }
        if (!root) {
            root = r;
        }
        new = r->data;
        d = (void*)(new + 1);
        node_translate(new, cur->position);
        node_rotate_q(new, cur->orientation);

        switch (cur->type) {
            case NODE_EMPTY:
            case NODE_BONE:
                break;
            case NODE_GEOMETRY:
                if (!variable_make_vertex_array(shell, cur->data.geometry->vertexArray, &d->data.geom.vertexArray)) {
                    shell_decref(root);
                    return 0;
                }
                if (!variable_make_material(shell, cur->data.geometry->material, &d->data.geom.material)) {
                    variable_free(&d->data.geom.vertexArray);
                    shell_decref(root);
                    return 0;
                }
                d->data.geom.geometry.vertexArray = variable_to_vertex_array(&d->data.geom.vertexArray);
                d->data.geom.geometry.material = variable_to_material(&d->data.geom.material);
                node_set_geometry(new, &d->data.geom.geometry);
                break;
            case NODE_DLIGHT:
                if (!variable_make_dlight(shell, cur->data.dlight, &d->data.light)) {
                    shell_decref(root);
                    return 0;
                }
                node_set_dlight(new, variable_to_dlight(&d->data.light));
                break;
            case NODE_PLIGHT:
                if (!variable_make_plight(shell, cur->data.plight, &d->data.light)) {
                    shell_decref(root);
                    return 0;
                }
                node_set_plight(new, variable_to_plight(&d->data.light));
                break;
            case NODE_SLIGHT:
                if (!variable_make_slight(shell, cur->data.slight, &d->data.light)) {
                    shell_decref(root);
                    return 0;
                }
                node_set_slight(new, variable_to_slight(&d->data.light));
                break;
            case NODE_CAMERA:
                if (!variable_make_camera(shell, cur->data.camera, &d->data.camera)) {
                    shell_decref(root);
                    return 0;
                }
                {
                    struct Node** cnode;
                    if (!(cnode = variable_to_camera_node_ptr(&d->data.camera))) {
                        variable_free(&d->data.camera);
                        shell_decref(root);
                        return 0;
                    }
                    *cnode = new;
                }
                node_set_camera(new, variable_to_camera(&d->data.camera));
                break;
            default:
                shell_decref(root);
                return 0;
        }
        if (new->nbChildren < cur->nbChildren) {
            next = cur->children[new->nbChildren];
            parent = r;
        } else if (cur == node) {
            next = NULL;
        } else {
            for (next = cur->father; next; next = next->father) {
                if (((struct Node*)parent->data)->nbChildren < next->nbChildren) {
                    next = next->children[((struct Node*)parent->data)->nbChildren];
                    break;
                } else if (next == node) {
                    next = NULL;
                    break;
                }
                r = parent;
                parent = ((struct NodeVarData*)(((struct Node*)r->data)->father + 1))->self;
            }
        }
    }
    if (!(sinfo = node_struct_type(node->type))) {
        shell_decref(root);
        return 0;
    }
    variable_make_struct(sinfo, root, dest);
    return 1;
}
