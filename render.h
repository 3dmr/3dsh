#include "shell.h"

#ifndef TDSH_RENDER_H
#define TDSH_RENDER_H

int update_scene(struct DemoShell* shell);

int render_frame(struct DemoShell* shell);

void* shell_frame_swap_thread(void* data);

#endif
